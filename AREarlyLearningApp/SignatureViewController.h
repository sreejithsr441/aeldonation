//
//  SignatureViewController.h
//  AREarlyLearningApp
//
//  Created by Sreejith Rajan on 08/03/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PJRSignatureView.h"
#import "MBProgressHUD.h"

@protocol dailReportDelegateData
@optional
-(void)CallTransaction;
@end

@interface SignatureViewController : UIViewController<UITextFieldDelegate>
{
  PJRSignatureView *signatureView;
    NSMutableDictionary *dictToSend;
    MBProgressHUD *HUD;
    NSString *totalAmntOftransaction;
    NSString *specialNote;
    NSMutableArray *ArrOfflineData;
    BOOL Reachability;
    BOOL EmailValidationRequired;
    
}
@property (weak, nonatomic) IBOutlet UIButton *btnCheckMark;
- (IBAction)BtnCheckMarkClicked:(id)sender;


@property(strong,nonatomic) NSArray *ArrData;
@property (weak, nonatomic) IBOutlet UIButton *btnClear;
- (IBAction)BtnClearClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
- (IBAction)BtnSaveClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
- (IBAction)BtnCancelClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *LblEmail;
@property(strong,nonatomic) NSDictionary *dicDonor;
@property(strong,nonatomic) NSMutableArray *ArrToServer;
@property(strong,nonatomic) NSString *StrCurrentdate;
@property(strong,nonatomic) NSString *specialNote;
@property(nonatomic,retain)id<dailReportDelegateData>ReportDelegateTransaction;
//@property(nonatomic,retain) id<tableReloadDelegate>DelegateTableReload;
@end
