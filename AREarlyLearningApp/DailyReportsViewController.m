//
//  DailyReportsViewController.m
//  AREarlyLearningApp
//
//  Created by Sabin.MS on 26/03/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import "DailyReportsViewController.h"
#import "SRUtility.h"
#import "SRTableViewCell.h"
#import "BasciHistoryObject.h"
#import "SRDailyReprtsTableViewCell.h"
@interface DailyReportsViewController ()

@end

@implementation DailyReportsViewController
@synthesize ReportDelegate;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    NSData *DataOffline=[SRUtility getdatafromplist:@"OfflineTransaction" plist:@"Data"];
    ArrForSavingData=[NSKeyedUnarchiver unarchiveObjectWithData:DataOffline];
    NSDate *Now=[NSDate date];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EEEE,MMMM dd,yyyy"];
    NSDateFormatter *DateFormatteToSnd=[[NSDateFormatter alloc]init];
    [DateFormatteToSnd setDateFormat:@"yyyy-MM-dd"];
    NSString *StrDateToSnd=[DateFormatteToSnd stringFromDate:Now];
    NSString *StrDate=[dateFormatter stringFromDate:Now];
    [_LblDate setText:StrDate];
    BtnClickedCount=1;
    btnCLickedAddCount=1;
    [self dailyReports:StrDateToSnd];
   // [self DailyRprtFrmService:StrDateToSnd];
    data=[NSDate date];
    yesterday=[NSDate date];
    
    // Do any additional setup after loading the view.
}


-(void)dailyReports:(NSString*)StrDate
{
    

    ArrTBldailyRprts=[[NSMutableArray alloc]init];
    float GrandTotal = 0;
    for (int i=0; i<[ArrForSavingData count]; i++)
    {
        NSString *StrCurrentDate=[[[[ArrForSavingData objectAtIndex:i] objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"date_time"];
        NSArray *ArrDateComponents=[StrCurrentDate componentsSeparatedByString:@" "];
        NSString *Strdate=ArrDateComponents[0];
        if ([Strdate isEqualToString:StrDate])
        {
            [ArrTBldailyRprts addObject:[ArrForSavingData objectAtIndex:i]];
            NSString *StrVale=[NSString stringWithFormat:@"%@",[[[[ArrForSavingData objectAtIndex:i] objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"grand_total"]];
            float SumTotal=[StrVale floatValue];
            GrandTotal=GrandTotal+SumTotal;
        }
        
    }
              
    for (int i=0; i<[ArrTBldailyRprts count]; i++)
    {
        if (![[[[[ArrTBldailyRprts objectAtIndex:i] objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"transactionID"] isEqualToString:@""])
        {
            NSString *strTransid=[NSString stringWithFormat:@"%@",[[[[ArrTBldailyRprts objectAtIndex:i] objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"transactionID"]];
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;
            NSNumber *numTransId=[f numberFromString:strTransid];
            [[ArrTBldailyRprts objectAtIndex:i] setObject:numTransId forKey:@"transactionID"];
        }
        
       
    }
    
    
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]
                                      initWithKey: @"transactionID" ascending: YES];
    NSArray *ArrSort=[NSArray arrayWithObject:sortDescriptor];
    NSArray *arr=[ArrTBldailyRprts sortedArrayUsingDescriptors:ArrSort];
    ArrTBldailyRprts=[[NSMutableArray alloc] initWithArray:arr];
    //[ArrTBldailyRprts sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    TotalGrandTotal=GrandTotal;
    StrGrandTotal=[NSString stringWithFormat:@"%.2f",GrandTotal];
    [_LblTotal setText:StrGrandTotal];
    [_TblDailyRprts reloadData];
    

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backBtn:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:Nil];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BtnMinusClicked:(id)sender
{
    yesterday = [NSDate dateWithTimeInterval:-(60.0f*60.0f*24.0f)*BtnClickedCount sinceDate:data];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"EEEE,MMMM dd,yyyy"];
    NSDateFormatter *DateFormatteToSnd=[[NSDateFormatter alloc]init];
    [DateFormatteToSnd setDateFormat:@"yyyy-MM-dd"];
    NSString *StrDateToSnd=[DateFormatteToSnd stringFromDate:yesterday];
    NSString *StrDate=[df stringFromDate:yesterday];
    [self dailyReports:StrDateToSnd];
    [_LblDate setText:StrDate];
    BtnClickedCount++;
    BtnMinusClicked=YES;
    if (BtnClickedAdd==YES)
    {
        btnCLickedAddCount=0;
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat SectionHeight=0;
    SectionHeight=35.0;
    return SectionHeight;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{   UIView *ViewTmp;
   
    if (tableView==_TblDailyRprts)
    {   ViewTmp=[[UIView alloc]initWithFrame:CGRectMake(-2, 0, tableView.frame.size.width+4, 40)];
        [ViewTmp setBackgroundColor:[UIColor whiteColor]];
        UILabel *LblName=[[UILabel alloc]initWithFrame:CGRectMake(15, 2, 115, 40)];
        [LblName setText:@"Transaction No."];
        
        LblName.font=[UIFont fontWithName:@"Helvetica-Bold" size:12.0];
        [LblName setTextAlignment:NSTextAlignmentLeft];
        [ViewTmp addSubview:LblName];
        UILabel *LblQty=[[UILabel alloc]initWithFrame:CGRectMake(LblName.frame.origin.x+185, 2, 70, 40)];
        [LblQty setText:@"Name"];
        LblQty.font=[UIFont fontWithName:@"Helvetica-Bold" size:12.0];
        [LblQty setTextAlignment:NSTextAlignmentLeft];
        [ViewTmp addSubview:LblQty];
        UILabel *lblEach=[[UILabel alloc]initWithFrame:CGRectMake(LblQty.frame.origin.x+135, 2, 70, 40)];
        [lblEach setText:@"Donations"];
        lblEach.font=[UIFont fontWithName:@"Helvetica-Bold" size:12.0];
        [lblEach setTextAlignment:NSTextAlignmentLeft];
        [ViewTmp addSubview:lblEach];
        /*UILabel *LblTotal=[[UILabel alloc]initWithFrame:CGRectMake(lblEach.frame.origin.x+80, 2, 70, 40)];
        [LblTotal setText:@"Total"];
        LblTotal.font=[UIFont fontWithName:@"Helvetica-Bold" size:18.0];
        [LblTotal setTextAlignment:NSTextAlignmentLeft];
        [ViewTmp addSubview:LblTotal];*/
    }
    return ViewTmp;
}

- (IBAction)BtnAddClicked:(id)sender
{
    
    data=[NSDate dateWithTimeInterval:+(60.0f*60.0f*24.0f)*btnCLickedAddCount sinceDate:yesterday];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"EEEE,MMMM dd,yyyy"];
    NSString *StrDate=[df stringFromDate:data];
    [_LblDate setText:StrDate];
    NSDateFormatter *DateFormatteToSnd=[[NSDateFormatter alloc]init];
    [DateFormatteToSnd setDateFormat:@"yyyy-MM-dd"];
    NSString *StrDateToSnd=[DateFormatteToSnd stringFromDate:data];
    [self dailyReports:StrDateToSnd];
    btnCLickedAddCount++;
    BtnClickedAdd=YES;
    if (BtnMinusClicked==YES)
    {
        BtnClickedCount=0;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
   
    return [ArrTBldailyRprts count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SRDailyReprtsTableViewCell *cell=(SRDailyReprtsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"myidentifier"];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SRDailyReprtsTableViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    NSString *donationAmount=[[[[ArrTBldailyRprts objectAtIndex:indexPath.row]  objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"grand_total"];
    NSString *donationAmountMod=[NSString stringWithFormat:@"$ %@",donationAmount];
    [cell.LblDntions setText:donationAmountMod];
    [cell.LblNmer setText:[[[[ArrTBldailyRprts objectAtIndex:indexPath.row]   objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"donor_name"]];
    if ([[[ArrTBldailyRprts objectAtIndex:indexPath.row]  objectForKey:@"server_recieved"] isEqualToString:@"1"])
    {    [cell.LblTrnsction setText:[[[[ArrTBldailyRprts objectAtIndex:indexPath.row]   objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"transactionID"]];
        [cell.ImgStatus setImage:[UIImage imageNamed:@"tick_ar_learning.png"]];
    }
    else
    {
        [cell.LblTrnsction setText:@"..."];
       [cell.ImgStatus setImage:[UIImage imageNamed:@"cross_ar_learning.png"]];
    }
    return cell;
    
    
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    if ([[[ArrTBldailyRprts objectAtIndex:indexPath.row] objectForKey:@"server_recieved"]isEqualToString:@"0"])
    {
       
        
        return UITableViewCellEditingStyleDelete;
    }
    else
    {
        return UITableViewCellEditingStyleNone;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    
        
        
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {

        NSString *StrVale=[NSString stringWithFormat:@"%@",[[[[ArrTBldailyRprts objectAtIndex:indexPath.row] objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"grand_total"]];
        [ArrTBldailyRprts removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [_TblDailyRprts reloadData];
        ArrForSavingData=[ArrTBldailyRprts mutableCopy];
        NSData *dataDonations=[NSKeyedArchiver archivedDataWithRootObject:ArrForSavingData];
        [SRUtility adddatatoplist:dataDonations key:@"OfflineTransaction" plist:@"Data"];
        float GrandTotal = TotalGrandTotal;
        NSDictionary *dict=[[NSDictionary alloc]init];
        if (self.ReportDelegate)
        {
            [ReportDelegate reloadTableFromDailyReports:dict arrData:ArrTBldailyRprts];
        }
        NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
        [prefs setObject:@"FromDonationView" forKey:@"DonorSelectionTable"];
        [prefs synchronize];

         float SumTotal=[StrVale floatValue];
         GrandTotal=GrandTotal-SumTotal;
        TotalGrandTotal=GrandTotal;
        StrGrandTotal=[NSString stringWithFormat:@"%.2f",GrandTotal];
        [_LblTotal setText:StrGrandTotal];
        
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    NSDictionary *ArrIndex=[ArrTBldailyRprts objectAtIndex:indexPath.row];
    if ([[ArrIndex  objectForKey:@"server_recieved"]isEqualToString:@"0"])
    {
        if ([ArrIndex objectForKey:@"AlreadyDonated"])
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AREarlyLearning" message:@"Transaction already made waiting for the network to sync" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
        }
        
        else
        {
        
          if (self.ReportDelegate)
          {
               [ReportDelegate reloadTableFromDailyReports:ArrIndex arrData:ArrTBldailyRprts];
               float progress = 0.0;
               while (progress < 1.0)
               {
                    progress += 0.20;
                    HUD.progress = progress;
                    //usleep(50000);
                    HUD=[[MBProgressHUD alloc]initWithView:self.view];
                    [self.view addSubview:HUD];
                    [HUD show:YES];
                }
            
           }
           NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
           [prefs setObject:@"FromDailyReports" forKey:@"DonorSelectionTable"];
           [prefs synchronize];
           [self dismissViewControllerAnimated:YES completion:nil];
        }
    
    }
}



@end
