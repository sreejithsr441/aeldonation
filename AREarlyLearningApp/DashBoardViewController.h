//
//  DashBoardViewController.h
//  AREarlyLearningApp
//
//  Created by Sreejith Rajan on 02/03/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChangeProfilePicViewController.h"
#import "PopoverView.h"
#import "FormSheetViewController.h"
#import "DonationDetailedView.h"
#import "DonorDetailViewController.h"
#import "DonorDetailsViewController.h"
#import "OCCalendarViewController.h"
#import "DonorSignUpViewController.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "DailyReportsViewController.h"
@interface DashBoardViewController : UIViewController<ProfPicDelegate,PopoverViewDelegate,UITableViewDataSource,UITableViewDelegate,CategoryDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UISearchBarDelegate,DonateDataDelegate,OCCalendarDelegate,donorDelegate,donors,UITextFieldDelegate,UITextViewDelegate,dailReportDelegat>
{  OCCalendarViewController *calVC;
    NSArray *ArrPopData;
    PopoverView *pv;
    NSMutableArray *ArrTableData;
    float Valuesub;
    NSMutableArray *ArrCollData;
    DonationDetailedView *ObjDonDetailedView;
    BOOL ViewCreated;
    int collectionFlowTag;
     NSMutableArray *searchResults;
    NSString *StrTotal;
     NSString *splRqstNote;
    MBProgressHUD *HUD;
    Reachability *internetReachable;
    Reachability *reachability;
     NSMutableArray *ArrOfflineData;
    NSMutableArray *ArrForSavingData;
    NSString *stringOFTransaction;
    float TotalValueSum;
    BOOL savedTransaction;
}
@property (nonatomic, assign) BOOL hasInet;
@property (strong, nonatomic) IBOutlet UIButton *itemNavigationBtn;
@property (strong, nonatomic) IBOutlet UIButton *testButton;
@property(strong,nonatomic) NSDictionary*donorDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnUserNme;
- (IBAction)BtnUserNmeClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnProPic;
- (IBAction)BtnProPicClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *btnCategory;
- (IBAction)BtnCategoryClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNotofication;
- (IBAction)BtnNotificationClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *TblViewDonation;
@property(strong,nonatomic) UITableView *TblViewUserPopUp;
@property (weak, nonatomic) IBOutlet UILabel *LblGrandTotal;
@property (weak, nonatomic) IBOutlet UIButton *btnDonate;
- (IBAction)BtnDonateClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *ClctnCategory;
@property (weak, nonatomic) IBOutlet UIView *ViewDonateDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnDailyRprs;
- (IBAction)BtnDailyReportClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnDonor;
- (IBAction)BtnDonorClicked:(id)sender;
@property(strong,nonatomic) UITableView *TableDonorDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnback;
- (IBAction)BackBtnClicked:(id)sender;
@property(strong,nonatomic) NSDictionary *dictDonorDetails;
@property (strong, nonatomic) IBOutlet UILabel *LblDonorName;
@property (strong, nonatomic) IBOutlet UILabel *LblTransaction;
@property (strong, nonatomic) IBOutlet UILabel *labltrasactionId;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property(strong,nonatomic) NSDictionary *dicUserDeatils;
- (IBAction)BtnSaveClicked:(id)sender;
@property BOOL offlineUserSignedUp;
@property BOOL transactionCompletedShown;
@end
