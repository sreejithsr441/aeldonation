//
//  DailyReportsViewController.h
//  AREarlyLearningApp
//
//  Created by Sabin.MS on 26/03/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
@protocol dailReportDelegat
@optional
-(void)reloadTableFromDailyReports:(NSDictionary*)ArrFromDailyReports arrData:(NSArray*)ArrdailyReport;
@end
@interface DailyReportsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    int BtnClickedCount;
    NSMutableArray *ArrTBldailyRprts;
    MBProgressHUD *HUD;
    NSString *StrGrandTotal;
    NSDate *yesterday;
    NSMutableArray *ArrForSavingData;
    int btnCLickedAddCount;
    BOOL BtnClickedAdd;
    NSDate *data;
    BOOL BtnMinusClicked;
    float Valuesub;
    float TotalGrandTotal;
    
}

@property (weak, nonatomic) IBOutlet UIButton *DailyReportBack;
@property (weak, nonatomic) IBOutlet UILabel *LblDate;
- (IBAction)BtnMinusClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
- (IBAction)BtnAddClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *TblDailyRprts;
@property (weak, nonatomic) IBOutlet UILabel *LblTotal;
@property(nonatomic,retain)id<dailReportDelegat>ReportDelegate;
@end
