//
//  ChangePassViewController.h
//  AREarlyLearningApp
//
//  Created by Sabin.MS on 28/03/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePassViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *LblChange;
@property (weak, nonatomic) IBOutlet UITextField *TxtChnge;
- (IBAction)BtnChangeClicked:(id)sender;
- (IBAction)BtnCancelClicked:(id)sender;
@property(strong,nonatomic) NSString *StrChange;
@property (weak, nonatomic) IBOutlet UIButton *btnChange;
@end
