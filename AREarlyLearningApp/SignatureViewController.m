//
//  SignatureViewController.m
//  AREarlyLearningApp
//
//  Created by Sreejith Rajan on 08/03/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import "SignatureViewController.h"
#import "SRUtility.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "ICHAlertView.h"
//#import "AESCrypt.h"
@interface SignatureViewController () {
    BOOL isDismissingViewController;
    AppDelegate *delegate;
    DashBoardViewController *parentViewController;
}
@property (strong, nonatomic) IBOutlet UIView *signaturePadView;

@end

@implementation SignatureViewController
@synthesize ArrData,specialNote,ReportDelegateTransaction;
- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    parentViewController = delegate.dashBoardViewController;
    isDismissingViewController = NO;
    //signatureView= [[PJRSignatureView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x+30,90, self.view.frame.size.width-590, self.view.frame.size.height-350)];
    signatureView = [[PJRSignatureView alloc] initWithFrame:_signaturePadView.frame];
    signatureView.layer.borderColor=[UIColor blackColor].CGColor;
    signatureView.layer.borderWidth=1.0;
    [self.view addSubview:signatureView];
    //[self.view sendSubviewToBack:signatureView];
    if ([_dicDonor objectForKey:@"email"])
     [_LblEmail setText:[[[[_ArrToServer objectAtIndex:0] objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"donor_email"]];
    NSData *data=[SRUtility getdatafromplist:@"Donations" plist:@"Data"];
    ArrOfflineData=[NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (![data length]||[ArrOfflineData count]==0)
    {
        ArrOfflineData=[[NSMutableArray alloc]init];
        
    }
    [_btnCheckMark setBackgroundImage:[UIImage imageNamed:@"withtick_25x25.png"] forState:UIControlStateSelected];
    [_btnCheckMark setTitle:@"WithTick" forState:UIControlStateSelected];
    _btnCheckMark.titleLabel.hidden=true;
    [_btnCheckMark setTitle:@"withtick_25x25.png" forState:UIControlStateSelected];
  //  EmailValidationRequired=YES;
    [_btnCheckMark setBackgroundImage:[UIImage imageNamed:@"without_tick.png"] forState:UIControlStateNormal];
    [_btnCheckMark setTitle:@"WithOutTick" forState:UIControlStateNormal];
    EmailValidationRequired=YES;
   //Reachability=reach;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:NO];
}

-(void)viewWillAppear:(BOOL)animated {
    _btnCheckMark.selected = YES;
    [super viewWillAppear:NO];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BtnClearClicked:(id)sender
{
     [signatureView clearSignature];
}

// PJRSignature code for getting image from a view was broken as it could not
// get the correct size of this popover view, thus causing some buggy visuals
// So, we use this simple workaround
- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0f);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:NO];
    UIImage * snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshotImage;
}

- (IBAction)BtnSaveClicked:(id)sender
{
    UIImage *SigImage=[self imageWithView:signatureView];
    if (!SigImage && ![_LblEmail.text length] && EmailValidationRequired)
    {
        UIAlertView *allert=[[UIAlertView alloc]initWithTitle:@"Donate" message:@"Please enter email address and sign here" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [allert show];
    } else
    {
        if (!SigImage)
        {
            UIAlertView *allert=[[UIAlertView alloc]initWithTitle:@"Donate" message:@"Please sign here" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [allert show];
        } else
        {

            if (EmailValidationRequired==YES)
            {
                [[[[_ArrToServer objectAtIndex:0] objectForKey:@"donors"] objectAtIndex:0]  removeObjectForKey:@"donor_email"];
                [[[[_ArrToServer objectAtIndex:0] objectForKey:@"donors"] objectAtIndex:0]  setObject:_LblEmail.text forKey:@"donor_email"];
            } else
            {
                [[[[_ArrToServer objectAtIndex:0] objectForKey:@"donors"] objectAtIndex:0]  removeObjectForKey:@"donor_email"];
                [[[[[_ArrToServer objectAtIndex:0] objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"donor_email"] setObject:@"" forKey:@"donor_email"];
             
            }
            
       
            NSString *imageConvString=[self imageToNSString:SigImage];
            NSString *strFltrdString=[imageConvString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            [[[[_ArrToServer objectAtIndex:0]objectForKey:@"donors"]objectAtIndex:0]setObject:strFltrdString forKey:@"donor_signature"];

            NSError *error;
            NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:_ArrToServer options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
          //  NSString *StrValue=[NSString stringWithFormat:@"data=%@",jsonString];
            NSString *StringToPass=[jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];
            if (EmailValidationRequired==YES)
            {
               if([_LblEmail.text length])
               {
                   [self WebServieceCall:StringToPass Array:_ArrToServer];
                } else
                {
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Donate" message:@"Please enter email address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                   [alert show];
                }
            }
            else
            {
                [self WebServieceCall:StringToPass Array:_ArrToServer];
            }  
        }
    }
}

-(void)WebServieceCall:(NSString*)StrValueToServer Array:(NSMutableArray*)ArryToPass
{
    if ([SRUtility reachable])
    {
        __block NSDictionary *dicWebResponse;
        HUD=[[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:HUD];
        
        [HUD showAnimated:YES whileExecutingBlock:^(void)
        {
           
             dicWebResponse=[SRUtility makeWebServiceCallForPOSTMethod:[NSURL URLWithString:[NSString stringWithFormat:@"http://donations.arearlylearning.org/donation-api/transaction.php"]] withAppendData:StrValueToServer withRequestHeaderOne:@"User-Agent" withRequestHeaderTwo:@"Content-Type" withHeaderValueOne:@"ASIHTTPRequest" withHeaderValueTwo:@"application/json"];
        }
        completionBlock:^(void)
        {
            
            
            
            if ([dicWebResponse objectForKey:@"status_message"])
             {
                 
                 if ([[dicWebResponse objectForKey:@"status_message"] isEqualToString:@"transaction insertion failed"])
                 {
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AREarlyLearningApp" message:[dicWebResponse objectForKey:@"status_message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alert show];
                     
                 }
                 else
                 {
                 
                     
                     [self dismissViewControllerAnimated:YES completion:^
                      {
                          
                          NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
                          [prefs setObject:@"FromDonationView" forKey:@"DonorSelectionTable"];
                          [prefs synchronize];
                          NSData *DataOffline=[SRUtility getdatafromplist:@"OfflineTransaction" plist:@"Data"];
                          NSMutableArray*ArrForSavingData=[NSKeyedUnarchiver unarchiveObjectWithData:DataOffline];
                          NSString *StrDatenTimeTopass=[[[[[dicWebResponse objectForKey:@"data"] objectAtIndex:0] objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"date_time"];
                          
                          for (int i=0; i<[ArrForSavingData count]; i++) {
                              if ([[[[[ArrForSavingData objectAtIndex:i] objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"date_time"] isEqualToString:StrDatenTimeTopass]) {
                                  [ArrForSavingData removeObjectAtIndex:i];
                                  [ArrForSavingData insertObject:[[dicWebResponse objectForKey:@"data"] objectAtIndex:0] atIndex:i];
                              }
                          }
                          
                          if (self.ReportDelegateTransaction) {
                              [self.ReportDelegateTransaction CallTransaction];
                          }
                          
                          NSData *data=[NSKeyedArchiver archivedDataWithRootObject:ArrForSavingData];
                          [SRUtility adddatatoplist:data key:@"OfflineTransaction" plist:@"Data"];
                          [[NSNotificationCenter defaultCenter]postNotificationName:@"CollectionReload" object:nil];
                          [[NSNotificationCenter defaultCenter]postNotificationName:@"Reloadtable" object:nil];
                          
                          parentViewController.transactionCompletedShown = NO;
                          
                          
                          UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Donation" message:@"Transaction Successfully Completed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                          [alert show];
                          
                          
                      }];

                 
                 }
                 
                 
                 
                 
                 
                 
                 
             }
            else
            {
                 [self dismissViewControllerAnimated:YES completion:^ {
                     
                     [[NSNotificationCenter defaultCenter]postNotificationName:@"Reloadtable" object:nil];
                     [ArrOfflineData addObject:ArryToPass];
                     NSData *data=[NSKeyedArchiver archivedDataWithRootObject:ArrOfflineData];
                     [SRUtility adddatatoplist:data key:@"Donation" plist:@"Data"];
                     
                     parentViewController.transactionCompletedShown = NO;
                     
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Donation" message:@"Transaction Successfully Completed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alert show];
                 }];
                 
             }
         }];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:^
        {
            
            NSLog(@"ArrOfflineData : %@", ArrOfflineData);
            NSLog(@"ArryToPass : %@", ArryToPass);
                        [ArrOfflineData addObject:ArryToPass];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"Reloadtable" object:nil];
            NSData *data=[NSKeyedArchiver archivedDataWithRootObject:ArrOfflineData];
            [SRUtility adddatatoplist:data key:@"Donations" plist:@"Data"];
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Donation" message:@"Transaction processed.  The data will sync when network connection is restored." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }];
    }
}

- (IBAction)BtnCancelClicked:(id)sender
{   NSData *DataOffline=[SRUtility getdatafromplist:@"OfflineTransaction" plist:@"Data"];
    NSMutableArray*ArrForSavingData=[NSKeyedUnarchiver unarchiveObjectWithData:DataOffline];
    NSString *StrPinNmbr=[NSString stringWithFormat:@"%@",[[_ArrToServer objectAtIndex:0] objectForKey:@"pin_nbr"]];
    NSString *StrGrandTot=[NSString stringWithFormat:@"%@",[[[[_ArrToServer objectAtIndex:0] objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"grand_total"]];
    
    for (int i=0; i<[ArrForSavingData count]; i++) {
        if ([[[ArrForSavingData objectAtIndex:i]  objectForKey:@"pin_nbr"] isEqualToString:StrPinNmbr]&&[[[[[ArrForSavingData objectAtIndex:i]  objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"grand_total"] isEqualToString:StrGrandTot])
        {
            [ArrForSavingData removeObjectAtIndex:i];
           // [[ArrForSavingData objectAtIndex:i] removeObjectForKey:@"AlreadyDonated"];
            //[ArrOfflineData insertObject:ArryToPass atIndex:i];
        }
    }
    NSData *data=[NSKeyedArchiver archivedDataWithRootObject:ArrForSavingData];
    [SRUtility adddatatoplist:data key:@"OfflineTransaction" plist:@"Data"];
    
    [self dismissViewControllerAnimated:YES completion:Nil];
}

-(NSString *)imageToNSString:(UIImage *)image
{
    NSData *imageData = UIImagePNGRepresentation(image);
    return [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

- (IBAction)BtnCheckMarkClicked:(id)sender
{
    _btnCheckMark.selected=!_btnCheckMark.selected;
    if (_btnCheckMark.selected) {
       // _LblEmail.text=@"";
        [_btnCheckMark setTitle:@"WithTick" forState:UIControlStateNormal];
        EmailValidationRequired = YES;
        
    } else {
        //[_LblEmail setText:[_dicDonor objectForKey:@"email"]];
        [_btnCheckMark setTitle:@"WithOutTick" forState:UIControlStateSelected];
        EmailValidationRequired=NO;
    }
}
@end
