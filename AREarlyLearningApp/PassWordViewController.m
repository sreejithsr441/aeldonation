//
//  PassWordViewController.m
//  AREarlyLearningApp
//
//  Created by Sreejith Rajan on 02/03/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import "PassWordViewController.h"
//#import "BFPaperButton.h"
#import "UIColor+BFPaperColors.h"
#import "DashBoardViewController.h"
#import "LNNumberpad.h"
#import "SRUtility.h"
#import "Reachability.h"
@interface PassWordViewController ()
{
    BOOL loginSucees;
    NSMutableDictionary *dicToOffUser;
    NSMutableDictionary *datadictionary;
}

@end

@implementation PassWordViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    loginSucees=NO;
    _TxtPassCode.layer.borderWidth=1.0f;
    _TxtPassCode.layer.borderColor=[UIColor whiteColor].CGColor;
    [[UITextField appearance] setTintColor:[UIColor whiteColor]];
    
    
    NSData *dataUsers=[SRUtility getdatafromplist:@"OfflineSignUpUser" plist:@"Data"];
    NSUInteger datauseLength=[dataUsers length];
    if (datauseLength==0) {
        [self initWithUsers];
    }
    
    NSData *dataCity=[SRUtility getdatafromplist:@"CityNames" plist:@"Data"];
    NSUInteger cityLength=[dataCity length];
    if (cityLength==0) {
        [self initWithCity];
    }
   
    NSData *dataCategory=[SRUtility getdatafromplist:@"Categories" plist:@"Data"];
    NSUInteger categoryLength=[dataCategory length];
    if (categoryLength==0)
      [self InitdataCategory];
    NSData *dataSubCategory=[SRUtility getdatafromplist:@"SubCategories" plist:@"Data"];
    NSUInteger SubCategoryLength=[dataSubCategory length];
    if (SubCategoryLength==0)
        [self InitdataSubcategory];
    
    NSData *dataItems=[SRUtility getdatafromplist:@"Items" plist:@"Data"];
    NSUInteger dataItemsLenghth=[dataItems length];
    if ( dataItemsLenghth==0)
          [self InitDataItems];
    
    NSData *dataDonors=[SRUtility getdatafromplist:@"Donors" plist:@"Data"];
    NSUInteger dataDonorLength=[dataDonors length];
    if (dataDonorLength==0)
        [self InitDonorData];
    [self.TxtPassCode setValue:[UIColor whiteColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    NSData *DataPlaces=[SRUtility getdatafromplist:@"PlacesCentre" plist:@"Data"];
    NSUInteger PlaceLength=[DataPlaces length];
    if (PlaceLength==0)
        [self initWithPlaces];
    
    NSData *dataCenter=[SRUtility getdatafromplist:@"PlacesCentre2" plist:@"Data"];
    NSUInteger CenterLenght=[dataCenter length];
    if (CenterLenght==0)
       [self initWithCenter];
    
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    [prefs setObject:@"DonorNotSelected" forKey:@"DonorSelection"];
    [prefs synchronize];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
   // [self InitdataCategory];
    /*
    BFPaperButton *bfFlatSmart = [[BFPaperButton alloc] initWithFrame:CGRectMake(389, 551, 257, 40) raised:NO];
    [bfFlatSmart setTitle:@"Enter" forState:UIControlStateNormal];
    [bfFlatSmart setTitleFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:15.f]];
    bfFlatSmart.backgroundColor=[UIColor paperColorBrown400];
    bfFlatSmart.layer.cornerRadius=8.0;
    bfFlatSmart.layer.borderWidth=1.0;
    [bfFlatSmart setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [bfFlatSmart setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [bfFlatSmart addTarget:self action:@selector(EnterPassCode) forControlEvents:UIControlEventTouchUpInside];
    
    */
    
    //[self.view addSubview:bfFlatSmart];
   // _TxtPassCode.inputView=[LNNumberpad defaultLNNumberpad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

- (IBAction)enterPassCode:(id)sender {
    NSUserDefaults *Prefs=[NSUserDefaults standardUserDefaults];
    NSString *StrPasS=[Prefs objectForKey:@"PassCode"];
    
    if ([_TxtPassCode.text isEqualToString:StrPasS]) {
        DashBoardViewController *ObjDash=[self.storyboard instantiateViewControllerWithIdentifier:@"DashBoardViewController"];
        [self.navigationController pushViewController:ObjDash animated:YES];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Congrats! You have successfully logged in" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    } else {
        _TxtPassCode.text=@"";
        UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"The passcode you entered is incorrect." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [Alert show];
    }
}

-(void)EnterPassCode
{
    NSUserDefaults *Prefs=[NSUserDefaults standardUserDefaults];
    NSString *StrPasS=[Prefs objectForKey:@"PassCode"];
    
    if ([_TxtPassCode.text isEqualToString:StrPasS]) {
        DashBoardViewController *ObjDash=[self.storyboard instantiateViewControllerWithIdentifier:@"DashBoardViewController"];
        [self.navigationController pushViewController:ObjDash animated:YES];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Congrats! You have successfully logged in" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    } else {
        _TxtPassCode.text=@"";
        UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"The passcode you entered is incorrect." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [Alert show];
    }
    
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    textField.placeholder=Nil;
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-180,1024,714)]; //here taken -20 for example i.e. your view will be scrolled to -20. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,0,1024,714)];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)InitdataCategory
{   __block NSDictionary *dciWebresponse;
    HUD=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.labelText=@"Please Wait Loading Master Data";
    [HUD showAnimated:YES whileExecutingBlock:^(void)
     {
         NSString *Strurl=@"http://donations.arearlylearning.org/api/index.php?request=categories";
         NSURL *TheUrl=[NSURL URLWithString:Strurl];
         dciWebresponse=[SRUtility makeWebServicecallForGetmethod:TheUrl];
     }
      completionBlock:^(void)
     {   if ([[dciWebresponse objectForKey:@"status_message"] isEqualToString:@"success"])
         {   NSData *data=[NSKeyedArchiver archivedDataWithRootObject:dciWebresponse];
             [SRUtility adddatatoplist:data key:@"Categories" plist:@"Data"];
         }
      }];
}
-(void)InitdataSubcategory
{
    __block NSDictionary *dicWebresponse;
    HUD=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.labelText=@"Please Wait Loading Master Data";
    
    [HUD showAnimated:YES whileExecutingBlock:^(void)
     { NSString *StrUrl=@"http://donations.arearlylearning.org/api/index.php?request=subcategories";
         NSURL *TheUrl=[NSURL URLWithString:StrUrl];
         dicWebresponse=[SRUtility makeWebServicecallForGetmethod:TheUrl];
     }
      completionBlock:^(void)
     { if([[dicWebresponse objectForKey:@"status_message"] isEqualToString:@"success"])
      {
          NSData *data=[NSKeyedArchiver archivedDataWithRootObject:dicWebresponse];
          [SRUtility adddatatoplist:data key:@"SubCategories" plist:@"Data"];
      }
      
     }];
    
    
}
-(void)InitDataItems
{
    __block NSDictionary *dicWebresponse;
    HUD=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.labelText=@"Please Wait Loading Master Data";
    [HUD showAnimated:YES whileExecutingBlock:^(void)
     {
         
         NSString *StrUrl=@"http://donations.arearlylearning.org/api/index.php?request=items";
         NSURL *TheUrl=[NSURL URLWithString:StrUrl];
         HUD.labelText=@"Please Wait Laoding Master data";
        
              dicWebresponse=[SRUtility makeWebServicecallForGetmethod:TheUrl];
         
         
         
         
     }
      completionBlock:^(void)
     { if ([[dicWebresponse objectForKey:@"status_message"] isEqualToString:@"success"])
       {
         NSData *data=[NSKeyedArchiver archivedDataWithRootObject:dicWebresponse];
         [SRUtility adddatatoplist:data key:@"Items" plist:@"Data"];
       }
     
     }];
    
}
-(void)InitDonorData
{
    __block NSDictionary *dicWebResponse;
    HUD=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.labelText=@"Please Wait Loading Master data";
    [HUD showAnimated:YES whileExecutingBlock:^(void)
     {NSString *Strurl=@"http://donations.arearlylearning.org/api/index.php?request=donors";
         NSURL *TheUrl=[NSURL URLWithString:Strurl];
         dicWebResponse=[SRUtility makeWebServicecallForGetmethod:TheUrl];
         
     }
      completionBlock:^(void)
     {
         if ([[dicWebResponse objectForKey:@"status_message"] isEqualToString:@"success"])
         {
             NSData *dataDonors=[NSKeyedArchiver archivedDataWithRootObject:dicWebResponse];
             [SRUtility adddatatoplist:dataDonors key:@"Donors" plist:@"Data"];
             
         }
     
     }];
    
    
}
/*
-(void)initWithPlaces{
    
    __block NSDictionary *dicWebResponse;
    HUD=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.labelText=@"Please Wait Loading Master data";
    [HUD showAnimated:YES whileExecutingBlock:^(void)
     {NSString *Strurl=@"http://aceonetest.com/webservice/index.php?request=donors";
         NSURL *TheUrl=[NSURL URLWithString:Strurl];
         dicWebResponse=[SRUtility makeWebServicecallForGetmethod:TheUrl];
         
     }
      completionBlock:^(void)
     {
         if ([[dicWebResponse objectForKey:@"status_message"] isEqualToString:@"success"])
         {
             NSData *dataDonors=[NSKeyedArchiver archivedDataWithRootObject:dicWebResponse];
             [SRUtility adddatatoplist:dataDonors key:@"Donors" plist:@"Data"];
             
         }
         
     }];

    
}
*/

-(void)initWithPlaces{
    
    __block NSDictionary *dicWebResponse;
    HUD=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.labelText=@"Please Wait Loading Master data";
    [HUD showAnimated:YES whileExecutingBlock:^(void)
     {NSString *Strurl=@"http://donations.arearlylearning.org/api/index.php?request=state";
         NSURL *TheUrl=[NSURL URLWithString:Strurl];
         dicWebResponse=[SRUtility makeWebServicecallForGetmethod:TheUrl];
         
     }
      completionBlock:^(void)
     {
         if ([[dicWebResponse objectForKey:@"status_message"] isEqualToString:@"success"])
         {
             NSData *dataDonors=[NSKeyedArchiver archivedDataWithRootObject:dicWebResponse];
             [SRUtility adddatatoplist:dataDonors key:@"PlacesCentre" plist:@"Data"];
             
         }
         
     }];
    
    
}
-(void)initWithCenter{
__block NSDictionary *dicWebResponse;
HUD=[[MBProgressHUD alloc]initWithView:self.view];
[self.view addSubview:HUD];
HUD.labelText=@"Please Wait Loading Master data";
[HUD showAnimated:YES whileExecutingBlock:^(void)
 {NSString *Strurl=@"http://donations.arearlylearning.org/api/index.php?request=centre";
     NSURL *TheUrl=[NSURL URLWithString:Strurl];
     dicWebResponse=[SRUtility makeWebServicecallForGetmethod:TheUrl];
     
 }
  completionBlock:^(void)
 {
     if ([[dicWebResponse objectForKey:@"status_message"] isEqualToString:@"success"])
     {
         NSData *dataDonors=[NSKeyedArchiver archivedDataWithRootObject:dicWebResponse];
         [SRUtility adddatatoplist:dataDonors key:@"PlacesCentre2" plist:@"Data"];
         
     }
     
 }];


}

-(void)initWithCity{
    __block NSDictionary *dicWebResponse;
    HUD=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.labelText=@"Please Wait Loading Master data";
    [HUD showAnimated:YES whileExecutingBlock:^(void)
     {NSString *Strurl=@"http://donations.arearlylearning.org/api/index.php?request=city";
         NSURL *TheUrl=[NSURL URLWithString:Strurl];
         dicWebResponse=[SRUtility makeWebServicecallForGetmethod:TheUrl];
         
     }
      completionBlock:^(void)
     {
         if ([[dicWebResponse objectForKey:@"status_message"] isEqualToString:@"success"])
         {
             NSData *dataDonors=[NSKeyedArchiver archivedDataWithRootObject:dicWebResponse];
             [SRUtility adddatatoplist:dataDonors key:@"CityNames" plist:@"Data"];
             
         }
         
     }];
    
    
}

-(void)initWithUsers
{
    __block NSDictionary *dicWebResponse;
    HUD=[[MBProgressHUD alloc]init];
    [self.view addSubview:HUD];
    HUD.labelText=@"Please Wait Loading Master data";
    [HUD showAnimated:YES whileExecutingBlock:^(void){
    NSString *StrUrl=@"http://donations.arearlylearning.org/api/index.php?request=users&user_pin";
        NSURL *theURL=[NSURL URLWithString:StrUrl];
        dicWebResponse=[SRUtility makeWebServicecallForGetmethod:theURL];
    }
      completionBlock:^(void)
    {
          if ([[dicWebResponse objectForKey:@"status_message"]isEqualToString:@"success"]) {
              NSData *datausers=[NSKeyedArchiver archivedDataWithRootObject:dicWebResponse];
              [SRUtility adddatatoplist:datausers key:@"OfflineSignUpUser" plist:@"Data"];
          }
      }
     ];
}



- (IBAction)BtnEnterClicked:(id)sender
{
  /*  NSData *DataDonors=[SRUtility getdatafromplist:@"Donors" plist:@"Data"];
    NSDictionary *DicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:DataDonors];
    NSMutableArray *DonorData=[DicTmp objectForKey:@"data"];
    NSPredicate *thePredicate=[NSPredicate predicateWithFormat:@"SELF.pin_nbr=%@",_TxtPassCode.text];
    NSArray *ArrFltrd=[DonorData filteredArrayUsingPredicate:thePredicate];
  
    
    
    if ([ArrFltrd count])
    {
        DashBoardViewController *ObjDash=[self.storyboard instantiateViewControllerWithIdentifier:@"DashBoardViewController"];
        ObjDash.dictDonorDetails=[ArrFltrd objectAtIndex:0];
        NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
        [prefs setObject:[[ArrFltrd objectAtIndex:0] objectForKey:@"pin_nbr"] forKey:@"UserPin"];
        [self.navigationController pushViewController:ObjDash animated:YES];
        
    }
    else
    {
        UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"AREarlyLearning" message:@"There is no such user" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [Alert show];

    }*/
    
   // NSUserDefaults *Prefs=[NSUserDefaults standardUserDefaults];
   /* NSString *StrPasS=[SRUtility getfromplist:@"Password" plist:@"Data"];
    if ([_TxtPassCode.text isEqualToString:StrPasS])
    {
        DashBoardViewController *ObjDash=[self.storyboard instantiateViewControllerWithIdentifier:@"DashBoardViewController"];
        [self.navigationController pushViewController:ObjDash animated:YES];
       
    }
    else
    {
        UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"AREarlyLearning" message:@"The PassCode You Entered is Wrong" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [Alert show];
    }*/
    //if ([SRUtility reachable]) {
        
    
    
    if([_TxtPassCode.text length])
    {
        if ([SRUtility reachable])
        {
            
        
        
        
        NSMutableDictionary *password=[[NSMutableDictionary alloc]init];
        [password setObject:_TxtPassCode.text forKey:@"user_pin"];
        //NSString *pintolog=_TxtPassCode.text;
    __block NSDictionary *dicWebResponse;
    HUD=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:HUD];
    [HUD showAnimated:YES whileExecutingBlock:^(void)
     {
         //NSLog(@"%@",_TxtPassCode.text);
         //dicWebResponse=[SRUtility makeWebServicecallForGetmethod:[NSURL URLWithString:[NSString stringWithFormat:@"http://aceonetest.com/webservice/index.php?request=users&user_pin=%@",pintolog]]];
         NSError *error;
         NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:password options:NSJSONWritingPrettyPrinted error:&error];
         NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
         // NSString *StrValue=[NSString stringWithFormat:@"data=%@",jsonString];
         NSString *StringToPass=[jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
         dicWebResponse=[SRUtility makeWebServiceCallForPOSTMethod:[NSURL URLWithString:[NSString stringWithFormat:@"http://donations.arearlylearning.org/api/login.php"]] withAppendData:StringToPass withRequestHeaderOne:@"User-Agent" withRequestHeaderTwo:@"Content-Type" withHeaderValueOne:@"ASIHTTPRequest" withHeaderValueTwo:@"application/json"];
        
       //  NSString *Strurl=[@"http://aceonetest.com/webservice/index.php?request=users&user_pin=1958"];
       //  NSURL *TheUrl=[NSURL URLWithString:Strurl];
        // dicWebResponse=[SRUtility makeWebServicecallForGetmethod:TheUrl];
     }
      completionBlock:^(void)
     {
         
         
         if([[dicWebResponse objectForKey:@"status_message"] isEqualToString:@"success"])
         {

        
         NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
         [prefs setObject:[[[dicWebResponse objectForKey:@"data"] objectAtIndex:0] objectForKey:@"user_pin"] forKey:@"UserPinForLogin"];
         [prefs synchronize];
         DashBoardViewController *ObjDash=[self.storyboard instantiateViewControllerWithIdentifier:@"DashBoardViewController"];
         ObjDash.dicUserDeatils=dicWebResponse;
         [self.navigationController pushViewController:ObjDash animated:YES];
         }
         else
         {
             _TxtPassCode.text=@"";
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Wrong Password" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
             [alert show];
         }
     
     
     }];
        }
        else
        {
            NSData *dataOfflineUsers=[SRUtility getdatafromplist:@"OfflineSignUpUser" plist:@"Data"];
            NSUInteger lengthOfDataUsers=[dataOfflineUsers length];
            if (lengthOfDataUsers==0) {
                
            
            UIAlertView *allert=[[UIAlertView alloc]initWithTitle:@"" message:@"Network is not available, please try later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [allert show];
            }else{
                
                NSDictionary *offlineUsers=[NSKeyedUnarchiver unarchiveObjectWithData:dataOfflineUsers];
                NSMutableArray *arrayOfUsers=[offlineUsers objectForKey:@"data"];
                /*
                NSPredicate *ThePredicate=[NSPredicate predicateWithFormat:@"SELF.user_pin=%@",_TxtPassCode.text];
                NSArray *ArrFltrd=[arrayOfUsers filteredArrayUsingPredicate:ThePredicate];
                if ([ArrFltrd count]) {
                    NSLog(@"alteredmethode");
                }
                 */
                
                for (int i=0;i<[arrayOfUsers count]; i++) {
                    if ([[[arrayOfUsers objectAtIndex:i]objectForKey:@"user_pin"]isEqualToString:_TxtPassCode.text])
                    {
                        
                        loginSucees=YES;
                        dicToOffUser=[arrayOfUsers objectAtIndex:i];
                        NSMutableArray *arrdictToOffUser=[[NSMutableArray alloc]initWithObjects:dicToOffUser, nil];
                       datadictionary=[[NSMutableDictionary alloc]init];
                        
                        [datadictionary setObject:arrdictToOffUser forKey:@"data"];
                        NSString *status=@"200";
                        NSString *status_message=@"status_message";
                        [datadictionary setObject:status forKey:@"status"];
                        [datadictionary setObject:status_message forKey:@"status_message"];
                        
                        NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
                        
                        [prefs setObject:[[[datadictionary objectForKey:@"data"] objectAtIndex:0] objectForKey:@"user_pin"] forKey:@"UserPinForLogin"];
                        [prefs synchronize];
                        break;
                     
                    }
                    else
                    {
                        loginSucees=NO;
                    }
                   
                }
                if (loginSucees==YES)
                {


                     DashBoardViewController *ObjDash=[self.storyboard instantiateViewControllerWithIdentifier:@"DashBoardViewController"];
                    ObjDash.dicUserDeatils=datadictionary;
                    [self.navigationController pushViewController:ObjDash animated:YES];
                }
                else{
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Wrong Password" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];

                }
            }
        }
    }
    
    
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Please enter the password" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    
    
}
@end
