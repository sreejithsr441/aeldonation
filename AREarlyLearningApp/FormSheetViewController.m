//
//  FormSheetViewController.m
//  AREarlyLearningApp
//
//  Created by Sreejith Rajan on 04/03/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import "FormSheetViewController.h"
#import "LNNumberpad.h"
#import "AppDelegate.h"
#import "DashBoardViewController.h"
#define ACCEPTABLE_CHARECTERS @"0123456789."

@interface FormSheetViewController ()

@end

@implementation FormSheetViewController

@synthesize StrCategory,categoryChange,strProDescp,dicItem;

- (void)viewDidLoad {
    [super viewDidLoad];
    //    NSDictionary *dict=[dicItem mutableCopy];
    [_LblCategory setText:StrCategory];
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    StrDataOverride=[prefs objectForKey:@"OverRideViewController"];
    
    if ([StrDataOverride isEqualToString:@"OverRiding"]) {
        [_LblDisplay setText:@"Enter Amount"];
        [_btnDonate setBackgroundImage:[UIImage imageNamed:@"ok"] forState:UIControlStateNormal];
        [_TxtAmnt setText:[dicItem objectForKey:@"dollOne"]];
    } else if([StrDataOverride isEqualToString:@"Nothing"]) {
        [_LblDisplay setText:@"Enter Amount"];
        [_btnDonate setBackgroundImage:[UIImage imageNamed:@"ok"] forState:UIControlStateNormal];
    } else if ([StrDataOverride isEqualToString:@"ModifyQuantity"]) {
        [_LblDisplay setText:@"Enter Quantity"];
        [_btnDonate setBackgroundImage:[UIImage imageNamed:@"ok"] forState:UIControlStateNormal];
    }
   // _TxtAmnt.inputView=[LNNumberpad defaultLNNumberpad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}*/

- (IBAction)BtnDonateClicked:(id)sender {
    if ([_TxtAmnt.text length]) {
        if([StrDataOverride isEqualToString:@"OverRiding"]) {
           NSMutableDictionary *dictValCate=[[NSMutableDictionary alloc]init];
           [dictValCate setObject:[dicItem objectForKey:@"image"] forKey:@"image"];
           [dictValCate setObject:[dicItem objectForKey:@"item_id"] forKey:@"item_id"];
           [dictValCate setObject:[dicItem objectForKey:@"TblMaincontent"] forKey:@"TblMaincontent"];
           if ([dicItem objectForKey:@"itemDescription"]) {
               [dictValCate setObject:[dicItem objectForKey:@"itemDescription"] forKey:@"itemDescription"];
           }
           [dictValCate setObject:@"FromOverridePrice" forKey:@"OverRidePrice"];
           [dictValCate setObject:[dicItem objectForKey:@"value"] forKey:@"value"];
           float value=[_TxtAmnt.text floatValue];
           float OverrRide=[[dicItem objectForKey:@"value"] floatValue];
           float TotalAmnt=value*OverrRide;
           NSString *decimalNumber=[NSString stringWithFormat:@"%.02f",value];
           NSString *decimalTotal=[NSString stringWithFormat:@"%.02f",TotalAmnt];
           [dictValCate setObject:decimalNumber forKey:@"dollOne"];
           [dictValCate setObject:decimalTotal forKey:@"dollTwo"];
           if (self.categoryChange) [categoryChange ReloadTablFromOtherView:dictValCate CellTag:_CellTag];
           [self dismissViewControllerAnimated:YES completion:nil];
        } else if([StrDataOverride isEqualToString:@"Nothing"]) {
            NSMutableDictionary *dictValCate=[[NSMutableDictionary alloc]init];
            [dictValCate setObject:[dicItem objectForKey:@"category_image"] forKey:@"image"];
            [dictValCate setObject:[dicItem objectForKey:@"item_id"] forKey:@"item_id"];
            [dictValCate setObject:_StrProName forKey:@"TblMaincontent"];
            [dictValCate setObject:strProDescp forKey:@"itemDescription"];
            [dictValCate setObject:@"1" forKey:@"value"];
            float value=[_TxtAmnt.text floatValue];
            NSString *decimalNumber=[NSString stringWithFormat:@"%.02f",value];
            [dictValCate setObject:decimalNumber forKey:@"dollOne"];
            [dictValCate setObject:decimalNumber forKey:@"dollTwo"];
            if (self.categoryChange) {
                [categoryChange ReloadTablFromOtherView:dictValCate CellTag:@""];
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        } else if ([StrDataOverride isEqualToString:@"ModifyQuantity"]) {
            NSMutableDictionary *dictValCate=[[NSMutableDictionary alloc]init];
            [dictValCate setObject:[dicItem objectForKey:@"image"] forKey:@"image"];
            [dictValCate setObject:[dicItem objectForKey:@"item_id"] forKey:@"item_id"];
            [dictValCate setObject:[dicItem objectForKey:@"TblMaincontent"] forKey:@"TblMaincontent"];
            if ([dicItem objectForKey:@"itemDescription"]) {
                [dictValCate setObject:[dicItem objectForKey:@"itemDescription"] forKey:@"itemDescription"];
            }
            float value=[_TxtAmnt.text floatValue];
             NSString *decimalNumber=[NSString stringWithFormat:@"%.02f",value];
            [dictValCate setObject:@"ModifyQuantity" forKey:@"Modifier"];
            [dictValCate setObject:[dicItem objectForKey:@"dollOne"] forKey:@"dollOne"];
            [dictValCate setObject:[dicItem objectForKey:@"dollTwo"] forKey:@"dollTwo"];
            [dictValCate setObject:decimalNumber forKey:@"value"];
            if (self.categoryChange) {
                [categoryChange ReloadTablFromOtherView:dictValCate CellTag:_CellTag];
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }  else {
            UIAlertView *allert1=[[UIAlertView alloc]initWithTitle:@"" message:@"Enter valid amount" delegate:self cancelButtonTitle:@"re-enter" otherButtonTitles:nil];
            [allert1 show];
        }
    } else {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Please enter the amount" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)BtnCancelClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        [_TxtAmnt setText:@""];
    }
}
/*
-(BOOL) TxtAmnt:(NSString*)newText
{
    BOOL result = false;
    
    if ( [newText containsString:@"^(?:|0|[1-9]\\d*)(?:\\.\\d*)?$"] )
    {
        result = true;
       // value = [newText doubleValue];
    }
    return result;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
    
    // The user deleting all input is perfectly acceptable.
    if ([resultingString length] == 0) {
        return true;
    }
    
    NSInteger holder;
    
    NSScanner *scan = [NSScanner scannerWithString: resultingString];
    
    return [scan scanInteger: &holder] && [scan isAtEnd];
}*/

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    if (textField==_TxtAmnt)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{   [_TxtAmnt setText:@""];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    textField.placeholder=Nil;
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    
    
    NSLog(@"%@",textField.text);
    // splRqstNote=textField.text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    
    
    return YES;
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,0,1024,714)]; //here taken -20 for example i.e. your view will be scrolled to -20. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    
    [self.view setFrame:CGRectMake(0,0,1024,714)];
}


@end
