//
//  ChangePassViewController.m
//  AREarlyLearningApp
//
//  Created by Sabin.MS on 28/03/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import "ChangePassViewController.h"
#import "SRUtility.h"
@interface ChangePassViewController ()

@end

@implementation ChangePassViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_LblChange setText:_StrChange];
    //[_btnChange setTitle:_StrChange forState:UIControlStateNormal];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BtnChangeClicked:(id)sender
{
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    NSString *StrPrefs=[prefs objectForKey:@"ChangeOfCharacter"];
    if ([StrPrefs isEqualToString:@"UserNameChange"])
    {
        [SRUtility addtoplist:_TxtChnge.text key:@"UserName" plist:@"Data"];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Username succesfully changed" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"UserNameChanged" object:Nil];
        
        [self dismissViewControllerAnimated:YES completion:Nil];
        
    }
    if ([StrPrefs isEqualToString:@"PassWordChange"])
    {
        [SRUtility addtoplist:_TxtChnge.text key:@"Password" plist:@"Data"];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR EarlyLearning" message:@"Password succesfully changed" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [self dismissViewControllerAnimated:YES completion:Nil];
    }
    
}

- (IBAction)BtnCancelClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:Nil];
}
@end
