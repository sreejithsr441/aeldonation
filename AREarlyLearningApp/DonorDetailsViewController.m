//
//  DonorDetailsViewController.m
//  AREarlyLearningApp
//
//  Created by Sreejith Rajan on 09/03/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import "DonorDetailsViewController.h"
#import "SRUtility.h"
#import "In_kind_volunteers.h"
#import "DonorSignUpViewController.h"
#import "FormSheetViewController.h"
@interface DonorDetailsViewController ()




@end

@implementation DonorDetailsViewController
@synthesize TblDonorDetails,searchBar,DonorChange;
- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    ArrData=[appDelegate getDonors];
    ObjDonorDetail=[[[NSBundle mainBundle]loadNibNamed:@"DonorInformationView" owner:self options:Nil] objectAtIndex:0];
    NSData *DataDonors=[SRUtility getdatafromplist:@"Donors" plist:@"Data"];
    NSDictionary *DicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:DataDonors];
    ArrTableData=[DicTmp objectForKey:@"data"];
    NSMutableDictionary *dictAddnewDonor=[[NSMutableDictionary alloc]init];
   [dictAddnewDonor setObject:@"+ Add New Donor" forKey:@"donor_name"];
   //[ArrTableData insertObject:dictAddnewDonor atIndex:0];
    //[ArrTableData ad];
  [ArrTableData addObject:dictAddnewDonor];
    
    NSData *Datacenter=[SRUtility getdatafromplist:@"PlacesCentre2" plist:@"Data"];
    NSDictionary *dictCenter=[NSKeyedUnarchiver unarchiveObjectWithData:Datacenter];
    ArrCenter=[dictCenter objectForKey:@"data"];
    
    
    NSData *DataState=[SRUtility getdatafromplist:@"PlacesCentre" plist:@"Data"];
    NSDictionary *dictState=[NSKeyedUnarchiver unarchiveObjectWithData:DataState];
    ArrState=[dictState objectForKey:@"data"];
               [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor blackColor]];
    for (UIView *view in searchBar.subviews){
        if ([view isKindOfClass: [UITextField class]]) {
            UITextField *tf = (UITextField *)view;
            tf.textColor=[UIColor blackColor];
            tf.delegate = self;
            break;
        }
    }
    
   // NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"donor_name" ascending:YES];
   // NSComparisonResult *compReslt=[[[ArrTableData objectAtIndex:0] objectForKey:@"donor_name"] ];
   // ArrTableData=[ArrTableData sortUsingDescriptors:@[sort]];
  // ArrTableData=[ArrTableData sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"donor_name" ascending:YES];
    [ArrTableData sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    
   
    [ObjDonorDetail.btnEdit setTitle:@"Edit" forState:UIControlStateNormal];
    
    [ObjDonorDetail.Txtname setHidden:YES];
    [ObjDonorDetail.TxtCellNum setHidden:YES];
    [ObjDonorDetail.btnStreet setHidden:YES];
    [ObjDonorDetail.btnState setHidden:YES];
    [ObjDonorDetail.btnCntry setHidden:YES];
    [ObjDonorDetail.btnDonatedDate setHidden:YES];
    [ObjDonorDetail.btnDonorSelect setHidden:NO];
    [ObjDonorDetail.editDonatedDate setHidden:YES];
    [ObjDonorDetail.editPin setHidden:YES];
    [ObjDonorDetail.editStreet setHidden:YES];
    [ObjDonorDetail.TxtEmailID setHidden:YES];
    [ObjDonorDetail.LblEmailID setHidden:NO];
    ObjDonorDetail.TxtEmailID.delegate=self;
    //[ObjDonorDetail.BtnCenter setHidden:YES];
    //[ObjDonorDetail.LblCenter setHidden:YES];
  
    //[ObjDonorDetail.LblNameCenter setHidden:YES];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger RowCount;
    switch (tableView.tag)
    {     case 0:
            RowCount=[ArrTableData count];
            break;
        case 1:
            RowCount=[ArrState count];
            break;
       case 2:
            RowCount=[ArrCenter count];
            break;
        default:
            break;
    }
    //else if (tableView==_TblViewCenter)
    //{
      //  count=[ArrCenter count];
   // }
    //else if (tableView==_TblState)
    //{
      //  count=[ArrState count];
     //}
    
    return RowCount;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"SRDonorCell"];
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SRDonorCell"];
    }
   // In_kind_volunteers *check=[ArrData objectAtIndex:indexPath.row];
   // NSString *StrFname=[NSString stringWithFormat:@"%@ ",check.fname];
   // NSString *StrFullName=[StrFname stringByAppendingString:check.lname];
    if (tableView.tag==0)
    {
        cell.textLabel.text=[[ArrTableData objectAtIndex:indexPath.row] objectForKey:@"donor_name"];
        cell.textLabel.font=[UIFont systemFontOfSize:21.0];
        cell.textLabel.textAlignment=NSTextAlignmentLeft;
    }
    if (tableView.tag==1)
    {
        cell.textLabel.text=[[ArrState objectAtIndex:indexPath.row] objectForKey:@"state"];
        cell.textLabel.font=[UIFont systemFontOfSize:21.0];
        cell.textLabel.textAlignment=NSTextAlignmentLeft;

    }
    if (tableView.tag==2)
    {   cell.textLabel.text=[[ArrCenter objectAtIndex:indexPath.row] objectForKey:@"centre"];
        cell.textLabel.font=[UIFont systemFontOfSize:18.0];
        cell.textLabel.lineBreakMode=UILineBreakModeWordWrap;
        cell.textLabel.numberOfLines=3;
        cell.textLabel.textAlignment=NSTextAlignmentLeft;

        
    }
    
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
    
    In_kind_volunteers *check=[ArrData objectAtIndex:indexPath.row];
    
    if (tableView.tag==0)
    {
      dictCurrent =[ArrTableData objectAtIndex:indexPath.row];
        if (indexPath.row==0)
    {
        if ([cell.textLabel.text isEqualToString:@"+ Add New Donor"])
        {
            [self dismissViewControllerAnimated:YES completion:Nil];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"DonorSignUpViewController" object:nil];

        }
        else
        {  ObjDonorDetail.LblName.text=[dictCurrent objectForKey:@"donor_name"];
            ObjDonorDetail.LblCellNum.text=[dictCurrent objectForKey:@"phone1"];
            ObjDonorDetail.LblPin.text=[dictCurrent objectForKey:@"zip"];
            ObjDonorDetail.LblStreet.text=[dictCurrent objectForKey:@"address"];
            ObjDonorDetail.LblState.text=[dictCurrent objectForKey:@"state"];
            ObjDonorDetail.LblCntry.text=check.cntry;
            ObjDonorDetail.LblDonatedDate.text=check.date;
            ObjDonorDetail.LblEmailID.text=[dictCurrent objectForKey:@"email"];
            [ObjDonorDetail.Txtname setText:[dictCurrent objectForKey:@"donor_name"]];
            [ObjDonorDetail.TxtEmailID setText:[dictCurrent objectForKey:@"email"]];
            //ObjDonorDetail.LblEmailID.text=[dictCurrent objectForKey:@"email"];
            [ObjDonorDetail.TxtCellNum setText:[dictCurrent objectForKey:@"phone1"]];
            //[ObjDonorDetail.LblDisplaycenter setText:[dictCurrent objectForKey:@"Center"]];
           // [ObjDonorDetail.LblStreet setText:[dictCurrent objectForKey:@"city"]];
            [ObjDonorDetail.btnDonatedDate setTitle:[dictCurrent objectForKey:@"date_time"] forState:UIControlStateNormal];
            [ObjDonorDetail.btnCntry setTitle:[dictCurrent objectForKey:@"state"] forState:UIControlStateNormal];
            [ObjDonorDetail.btnStreet setTitle:[dictCurrent objectForKey:@"address"] forState:UIControlStateNormal];
            [ObjDonorDetail.BtnCenter setTitle:[dictCurrent objectForKey:@"centre"] forState:UIControlStateNormal];
            [ObjDonorDetail.btnState setTitle:[dictCurrent objectForKey:@"state"] forState:UIControlStateNormal];
            [ObjDonorDetail.btnPin setTitle:[dictCurrent objectForKey:@"pin_nbr"] forState:UIControlStateNormal];
            [ObjDonorDetail.btnEdit setTitle:@"Edit" forState:UIControlStateNormal];
            [ObjDonorDetail.Txtname setHidden:YES];
            [ObjDonorDetail.TxtCellNum setHidden:YES];
            [ObjDonorDetail.btnStreet setHidden:YES];
            [ObjDonorDetail.btnState setHidden:YES];
            [ObjDonorDetail.btnCntry setHidden:NO];
            [ObjDonorDetail.editPin setHidden:YES];
            [ObjDonorDetail.TxtEmailID setHidden:YES];
            [ObjDonorDetail.editDonatedDate setHidden:YES];
            [ObjDonorDetail.editStreet setHidden:YES];
            [ObjDonorDetail.btnDonatedDate setHidden:YES];
            [ObjDonorDetail.btnDonorSelect setHidden:NO];
            [ObjDonorDetail.btnPin setHidden:YES];
            [ObjDonorDetail.LblName setHidden:NO];
            [ObjDonorDetail.LblCellNum setHidden:NO];
            [ObjDonorDetail.LblDonatedDate setHidden:NO];
            [ObjDonorDetail.LblState setHidden:NO];
            [ObjDonorDetail.LblStreet setHidden:NO];
            [ObjDonorDetail.LblState setHidden:NO];
            [ObjDonorDetail.LblCntry setHidden:NO];
            [ObjDonorDetail.LblEmailID setHidden:NO];
           
            //[ObjDonorDetail.LblNameCenter setHidden:NO];
            [ObjDonorDetail.LblPin setHidden:NO];
            [ObjDonorDetail.LblCenter setHidden:NO];
            
            
            
            
            
            [ObjDonorDetail.LblCenter setText:[dictCurrent objectForKey:@"centre"]];
            //[ObjDonorDetail.LblCenter setHidden:NO];
            [ObjDonorDetail.BtnCenter setHidden:YES];
            [ObjDonorDetail.btnBack addTarget:self action:@selector(BackBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
            [ObjDonorDetail.btnDonorSelect addTarget:self action:@selector(BtnSelect:) forControlEvents:UIControlEventTouchUpInside];
            [ObjDonorDetail.btnEdit addTarget:self action:@selector(BtnEdit:) forControlEvents:UIControlEventTouchUpInside];
            [ObjDonorDetail.btnCntry addTarget:self action:@selector(BtnCntry:) forControlEvents:UIControlEventTouchUpInside];
            [ObjDonorDetail.btnState addTarget:self action:@selector(btnState:) forControlEvents:UIControlEventTouchUpInside];
            [ObjDonorDetail.BtnCenter addTarget:self action:@selector(BtnCenter:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:ObjDonorDetail];
        
        }
       // FormSheetViewController *objFormSheet=[self.storyboard instantiateViewControllerWithIdentifier:@"FormSheetViewController"];
      //  [self.parentViewController presentViewController:objFormSheet animated:YES completion:Nil];
       // DonorSignUpViewController *ObjDonorSignup=[self.storyboard instantiateViewControllerWithIdentifier:@"DonorSignUpViewController"];
        //[self.parentViewController presentViewController:ObjDonorSignup animated:YES completion:Nil];
      
        
        
        
    }
    
    else
    {
    
        ObjDonorDetail.LblName.text=[dictCurrent objectForKey:@"donor_name"];
        ObjDonorDetail.LblCellNum.text=[dictCurrent objectForKey:@"phone1"];
        ObjDonorDetail.LblPin.text=[dictCurrent objectForKey:@"zip"];
        ObjDonorDetail.LblStreet.text=[dictCurrent objectForKey:@"address"];
        ObjDonorDetail.LblState.text=[dictCurrent objectForKey:@"state"];
        ObjDonorDetail.LblEmailID.text=[dictCurrent objectForKey:@"email"];
        ObjDonorDetail.LblCntry.text=check.cntry;
        ObjDonorDetail.LblDonatedDate.text=check.date;
        [ObjDonorDetail.LblCenter setText:[dictCurrent objectForKey:@"centre"]];
        
        
        
        [ObjDonorDetail.Txtname setText:[dictCurrent objectForKey:@"donor_name"]];
        [ObjDonorDetail.TxtEmailID setText:[dictCurrent objectForKey:@"email"]];
        //ObjDonorDetail.LblEmailID.text=[dictCurrent objectForKey:@"email"];
        [ObjDonorDetail.TxtCellNum setText:[dictCurrent objectForKey:@"phone1"]];
        [ObjDonorDetail.btnDonatedDate setTitle:[dictCurrent objectForKey:@"date_time"] forState:UIControlStateNormal];
        [ObjDonorDetail.btnCntry setTitle:[dictCurrent objectForKey:@"state"] forState:UIControlStateNormal];
        [ObjDonorDetail.btnStreet setTitle:[dictCurrent objectForKey:@"address"] forState:UIControlStateNormal];
        [ObjDonorDetail.btnState setTitle:[dictCurrent objectForKey:@"state"] forState:UIControlStateNormal];
        [ObjDonorDetail.btnPin setTitle:[dictCurrent objectForKey:@"pin_nbr"] forState:UIControlStateNormal];
        [ObjDonorDetail.btnEdit setTitle:@"Edit" forState:UIControlStateNormal];
        [ObjDonorDetail.Txtname setHidden:YES];
        [ObjDonorDetail.TxtCellNum setHidden:YES];
        [ObjDonorDetail.btnStreet setHidden:YES];
        [ObjDonorDetail.btnState setHidden:YES];
        [ObjDonorDetail.BtnCenter setHidden:YES];
        [ObjDonorDetail.btnCntry setHidden:NO];
        [ObjDonorDetail.editPin setHidden:YES];
        [ObjDonorDetail.TxtEmailID setHidden:YES];
        [ObjDonorDetail.editDonatedDate setHidden:YES];
        [ObjDonorDetail.editStreet setHidden:YES];
        [ObjDonorDetail.btnDonatedDate setHidden:YES];
        [ObjDonorDetail.btnDonorSelect setHidden:NO];
        [ObjDonorDetail.btnPin setHidden:YES];
        [ObjDonorDetail.LblName setHidden:NO];
        [ObjDonorDetail.LblCellNum setHidden:NO];
        [ObjDonorDetail.LblDonatedDate setHidden:NO];
        [ObjDonorDetail.LblState setHidden:NO];
        [ObjDonorDetail.LblStreet setHidden:NO];
        [ObjDonorDetail.LblState setHidden:NO];
        [ObjDonorDetail.LblCntry setHidden:NO];
        [ObjDonorDetail.LblEmailID setHidden:NO];
        [ObjDonorDetail.LblPin setHidden:NO];
        //[ObjDonorDetail.LblDisplaycenter setHidden:NO];
        //[ObjDonorDetail.LblDisplaycenter setHidden:NO];
        //[ObjDonorDetail.LblNameCenter setHidden:NO];
        [ObjDonorDetail.LblCenter setHidden:NO];
        [ObjDonorDetail.BtnCenter setHidden:YES];
       // [ObjDonorDetail.LblDisplaycenter setText:[dictCurrent objectForKey:@"Center"]];

    [ObjDonorDetail.btnBack addTarget:self action:@selector(BackBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
    [ObjDonorDetail.btnDonorSelect addTarget:self action:@selector(BtnSelect:) forControlEvents:UIControlEventTouchUpInside];
    [ObjDonorDetail.btnEdit addTarget:self action:@selector(BtnEdit:) forControlEvents:UIControlEventTouchUpInside];
        [ObjDonorDetail.btnCntry addTarget:self action:@selector(BtnCntry:) forControlEvents:UIControlEventTouchUpInside];
        [ObjDonorDetail.btnState addTarget:self action:@selector(btnState:) forControlEvents:UIControlEventTouchUpInside];
        [ObjDonorDetail.BtnCenter addTarget:self action:@selector(BtnCenter:) forControlEvents:UIControlEventTouchUpInside];
        
    [self.view addSubview:ObjDonorDetail];
    }
    }
    if (tableView.tag==1||tableView.tag==2)
    {
        [self popoverView:pv didSelectItemAtIndex:indexPath.row];
    }
    
}
-(IBAction)BtnCenter:(id)sender
{

    CGPoint point=ObjDonorDetail.BtnCenter.center;
    //state_Array=[[NSArray alloc] initWithObjects:@"A",@"B",@"C",@"D", nil];
    _TblViewCenter=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 250, 150)];
    _TblViewCenter.tag=2;
    _TblViewCenter.delegate=self;
    _TblViewCenter.dataSource = self;
    _TblViewCenter.scrollEnabled=YES;
    _TblViewCenter.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    pv=[PopoverView showPopoverAtPoint:point inView:self.view withContentView:_TblViewCenter delegate:self];
    pv.tag=200;

    
}

-(IBAction)BtnSelect:(id)sender
{
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    [prefs setObject:@"DonorSelected" forKey:@"DonorSelection"];
    [prefs synchronize];
    if (self.DonorChange) [DonorChange recieveDonorDetails:dictCurrent];
    [self dismissViewControllerAnimated:YES completion:Nil];
}
-(IBAction)BtnCntry:(id)sender
{

   // CGPoint point=ObjDonorDetail.btnCntry.center;
   // Arr=[[NSArray alloc]initWithObjects:@"America",@"India",@"Uganda",@"Africa",@"Israel",@"Palestine", nil];
   // pv=[PopoverView showPopoverAtPoint:point inView:self.view withStringArray:Arr delegate:self];
    
    
}
-(IBAction)btnState:(id)sender
{
    CGPoint point=ObjDonorDetail.btnState.center;
    //state_Array=[[NSArray alloc] initWithObjects:@"A",@"B",@"C",@"D", nil];
    _TblState=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 250, 150)];
    _TblState.tag=1;
    _TblState.delegate=self;
    _TblState.dataSource = self;
    _TblState.scrollEnabled=YES;
    _TblState.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    pv=[PopoverView showPopoverAtPoint:point inView:self.view withContentView:_TblState delegate:self];
    pv.tag=100;
}
-(void)popoverView:(PopoverView *)popoverView didSelectItemAtIndex:(NSInteger)index
{
    [popoverView performSelector:@selector(dismiss) withObject:nil afterDelay:0.3];
    if (pv.tag==100)
    {
        [ObjDonorDetail.btnState setTitle:[[ArrState objectAtIndex:index] objectForKey:@"state"]forState:UIControlStateNormal];
        
    }
    if (pv.tag==200)
    {
        [ObjDonorDetail.BtnCenter setTitle:[[ArrCenter objectAtIndex:index]objectForKey:@"centre"] forState:UIControlStateNormal];
    }
    
}

-(IBAction)BtnEdit:(id)sender
{
    if ([ObjDonorDetail.btnEdit.titleLabel.text isEqualToString:@"Edit"])
    {
        
        [ObjDonorDetail.btnEdit setTitle:@"Done" forState:UIControlStateNormal];
        [ObjDonorDetail.Txtname setHidden:NO];
        [ObjDonorDetail.TxtCellNum setHidden:NO];
        [ObjDonorDetail.btnStreet setHidden:NO];
        [ObjDonorDetail.btnState setHidden:NO];
        [ObjDonorDetail.btnCntry setHidden:NO];
        [ObjDonorDetail.btnDonatedDate setHidden:NO];
        [ObjDonorDetail.btnDonorSelect setHidden:YES];
        [ObjDonorDetail.editDonatedDate setHidden:NO];
        [ObjDonorDetail.editPin setHidden:NO];
        [ObjDonorDetail.editStreet setHidden:NO];
        [ObjDonorDetail.LblEmailID setHidden:NO];
        [ObjDonorDetail.TxtEmailID setHidden:NO];
        [ObjDonorDetail.BtnCenter setHidden:NO];
        
        [ObjDonorDetail.LblName setHidden:YES];
        [ObjDonorDetail.LblCellNum setHidden:YES];
        [ObjDonorDetail.LblDonatedDate setHidden:YES];
        [ObjDonorDetail.LblState setHidden:YES];
        [ObjDonorDetail.LblStreet setHidden:YES];
        [ObjDonorDetail.LblState setHidden:YES];
        [ObjDonorDetail.LblCntry setHidden:YES];
        [ObjDonorDetail.LblPin setHidden:YES];
        //[ObjDonorDetail.LblDisplaycenter setHidden:YES];
        //[ObjDonorDetail.LblNameCenter setHidden:NO];
        
        [ObjDonorDetail.Txtname setText:[dictCurrent objectForKey:@"donor_name"]];
        [ObjDonorDetail.TxtEmailID setText:[dictCurrent objectForKey:@"email"]];
        [ObjDonorDetail.editPin setText:[dictCurrent objectForKey:@"zip"]];
        [ObjDonorDetail.editStreet setText:[dictCurrent objectForKey:@"address"]];
        //ObjDonorDetail.LblEmailID.text=[dictCurrent objectForKey:@"email"];
        [ObjDonorDetail.TxtCellNum setText:[dictCurrent objectForKey:@"phone1"]];
        [ObjDonorDetail.btnDonatedDate setTitle:[dictCurrent objectForKey:@"date_time"] forState:UIControlStateNormal];
        [ObjDonorDetail.btnCntry setTitle:[dictCurrent objectForKey:@"state"] forState:UIControlStateNormal];
        //[ObjDonorDetail.btnStreet setTitle:[dictCurrent objectForKey:@"address"] forState:UIControlStateNormal];
        [ObjDonorDetail.btnState setTitle:[dictCurrent objectForKey:@"state"] forState:UIControlStateNormal];
        [ObjDonorDetail.btnPin setTitle:[dictCurrent objectForKey:@"pin_nbr"] forState:UIControlStateNormal];
        [ObjDonorDetail.BtnCenter setTitle:[dictCurrent objectForKey:@"centre"] forState:UIControlStateNormal];
        
        
        
        
    }
    else if ([ObjDonorDetail.btnEdit.titleLabel.text isEqualToString:@"Done"])
    {
        [ObjDonorDetail.btnEdit setTitle:@"Edit" forState:UIControlStateNormal];
        [ObjDonorDetail.Txtname setHidden:YES];
        [ObjDonorDetail.TxtCellNum setHidden:YES];
        [ObjDonorDetail.btnStreet setHidden:YES];
        [ObjDonorDetail.btnState setHidden:YES];
        [ObjDonorDetail.btnCntry setHidden:NO];
        [ObjDonorDetail.editPin setHidden:YES];
        [ObjDonorDetail.TxtEmailID setHidden:YES];
        [ObjDonorDetail.editDonatedDate setHidden:YES];
        [ObjDonorDetail.editStreet setHidden:YES];
        [ObjDonorDetail.btnDonatedDate setHidden:YES];
        [ObjDonorDetail.btnDonorSelect setHidden:NO];
        [ObjDonorDetail.btnPin setHidden:YES];
        [ObjDonorDetail.LblName setHidden:NO];
        [ObjDonorDetail.LblCellNum setHidden:NO];
        [ObjDonorDetail.LblDonatedDate setHidden:NO];
        [ObjDonorDetail.LblState setHidden:NO];
        [ObjDonorDetail.LblStreet setHidden:NO];
        [ObjDonorDetail.LblState setHidden:NO];
        [ObjDonorDetail.LblCntry setHidden:NO];
        [ObjDonorDetail.LblEmailID setHidden:NO];
        [ObjDonorDetail.BtnCenter setHidden:YES];
        [ObjDonorDetail.LblCenter setHidden:NO];
        [ObjDonorDetail.LblNameCenter setHidden:YES];
        [ObjDonorDetail.LblName setText:[dictCurrent objectForKey:@"donor_name"]];
        [ObjDonorDetail.LblCellNum setText:[dictCurrent objectForKey:@"phone1"]];
        [ObjDonorDetail.LblDonatedDate setText:[dictCurrent objectForKey:@"date_time"]];
        [ObjDonorDetail.LblEmailID setText:[dictCurrent objectForKey:@"email"]];
        [ObjDonorDetail.LblPin setText:[dictCurrent objectForKey:@"pin_nbr"]];
        [ObjDonorDetail.LblState setText:[dictCurrent objectForKey:@"state"]];
        [ObjDonorDetail.LblStreet setText:[dictCurrent objectForKey:@"city"]];
       //[ObjDonorDetail.LblCenter setText:[dictCurrent objectForKey:@"centre"]];
       // [ObjDonorDetail.LblDisplaycenter setText:[dictCurrent objectForKey:@"Center"]];
        [ObjDonorDetail.BtnCenter setTitle:[dictCurrent objectForKey:@"centre"] forState:UIControlStateNormal];
        [ObjDonorDetail.LblName setText:ObjDonorDetail.Txtname.text];
        [ObjDonorDetail.LblCellNum setText:ObjDonorDetail.TxtCellNum.text];
        [ObjDonorDetail.LblDonatedDate setText:ObjDonorDetail.editDonatedDate.text];
        [ObjDonorDetail.LblStreet setText:ObjDonorDetail.editStreet.text];
        [ObjDonorDetail.LblState setText:[ObjDonorDetail.btnState titleForState:UIControlStateNormal]];
        [ObjDonorDetail.LblCenter setText:ObjDonorDetail.BtnCenter.titleLabel.text];
        [ObjDonorDetail.LblPin setText:ObjDonorDetail.editPin.text];
        [ObjDonorDetail.LblEmailID setText:ObjDonorDetail.TxtEmailID.text];
       // [ObjDonorDetail.LblDisplaycenter setText:[ObjDonorDetail.BtnCenter titleForState:UIControlStateNormal]];
        
        ObjDonorDetail.TxtEmailID.delegate=self;
        NSMutableDictionary *dictDonor=[[NSMutableDictionary alloc]init];
        [dictDonor setObject:ObjDonorDetail.editStreet.text forKey:@"address"];
       // [dictDonor setObject:ObjDonorDetail. forKey:@"city"];
        //NSString *StrDonFNme=[NSString stringWithFormat:@"%@ ",_TxtFrstNme.text];
       // NSString *StrFullNme=[StrDonFNme stringByAppendingString:_TxtScndNme.text];
        [dictDonor setObject:ObjDonorDetail.Txtname.text forKey:@"donor_name"];
        [dictDonor setObject:ObjDonorDetail.TxtEmailID.text forKey:@"email"];
        [dictDonor setObject:ObjDonorDetail.TxtCellNum.text forKey:@"phone1"];
        
       // [dictDonor setObject:_TxtCell.text forKey:@"phone2"];
        [dictDonor setObject:[ObjDonorDetail.btnState titleForState:UIControlStateNormal] forKey:@"state"];
        [dictDonor setObject:ObjDonorDetail.editPin.text forKey:@"zip"];
        //if([ObjDonorDetail.BtnCenter titleForState:UIControlStateNormal])
        //NSString *centereString=ObjDonorDetail.BtnCenter.titleLabel.text;
       // NSLog(@"%@",centereString);
        [dictDonor setObject:ObjDonorDetail.BtnCenter.titleLabel.text forKey:@"centre"];
      //  [dictDonor setObject:[ObjDonorDetail.BtnCenter titleForState:UIControlStateNormal] forKey:@"centre"];
        NSError *error;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dictDonor options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        // NSString *StrValue=[NSString stringWithFormat:@"data=%@",jsonString];
        NSString *StringToPass=[jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];

        
        if ([SRUtility reachable])
        {   NSData *DataDonors=[SRUtility getdatafromplist:@"Donors" plist:@"Data"];
            NSDictionary *DicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:DataDonors];
            NSMutableArray *ArrDataValue=[DicTmp objectForKey:@"data"];
            NSPredicate *thePredicate=[NSPredicate predicateWithFormat:@"SELF.pin_nbr=%@",[dictCurrent objectForKey:@"pin_nbr"]];
            NSArray *Arrfltrd=[ArrDataValue filteredArrayUsingPredicate:thePredicate];
            [ArrDataValue removeObject:[Arrfltrd objectAtIndex:0]];
            
            __block NSDictionary *dicWebResponse;
            HUD=[[MBProgressHUD alloc]initWithView:self.view];
            [self.view addSubview:HUD];
            [HUD showAnimated:YES whileExecutingBlock:^(void)
             {//NSString *StrPin=[dictCurrent objectForKey:@"pin_nbr"];
               //  NSString *StrUrl=[NSString stringWithFormat:@"http://aceonetest.com/webservice/editdonor.php?id=%@",[dictCurrent objectForKey:@"pin_nbr"]];
                 dicWebResponse=[SRUtility makeWebServiceCallForPOSTMethod:[NSURL URLWithString:[NSString stringWithFormat:@"http://donations.arearlylearning.org/api/editdonor.php?id=%@",[dictCurrent objectForKey:@"pin_nbr"]]] withAppendData:StringToPass withRequestHeaderOne:@"User-Agent" withRequestHeaderTwo:@"Content-Type" withHeaderValueOne:@"ASIHTTPRequest" withHeaderValueTwo:@"application/json"];
             } completionBlock:^(void) {
                 if ([dicWebResponse objectForKey:@"status_message"])
                 {   //[[NSNotificationCenter defaultCenter]postNotificationName:@"ReloadSignup" object:dicWebResponse];
                    /*if (self.DonorChange)
                     {
                         [DonorChange recieveDonorDetails:[[dicWebResponse objectForKey:@"data"] objectAtIndex:0]];
                     }*/
                    [ArrDataValue addObject:[[dicWebResponse objectForKey:@"data"] objectAtIndex:0]];
//                     NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"donor_name" ascending:YES];
//                     [ArrDataValue sortUsingDescriptors:[NSArray arrayWithObject:sort]];

                     NSDictionary *dictData=[[NSDictionary alloc] initWithObjectsAndKeys:ArrDataValue, @"data",nil];
                     NSData *dataToSave=[NSKeyedArchiver archivedDataWithRootObject:dictData];
                     [SRUtility adddatatoplist:dataToSave key:@"Donors" plist:@"Data"];
                     
                     [self dismissViewControllerAnimated:YES completion:Nil];
                     
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Donor Successfully Updated" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alert show];
                     [self dismissViewControllerAnimated:YES completion:Nil];

                    // [[NSNotificationCenter defaultCenter]postNotificationName:@"DonorEdited" object:dicWebResponse];
                   if (self.DonorChange)
                     {
                         [DonorChange SignUpData:dicWebResponse];
                     }
                     
                     
                 }
             }
             ];
            
        }
        else
        {
            //[SRUtility addtoplist:StringToPass key:@"OfflinDonor" plist:@"Data"];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Donor will be added once network Connection is established" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
        }

        
    
    }
    

}

-(IBAction)BackBtnCLicked:(id)sender
{
       [ObjDonorDetail removeFromSuperview];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BtnCancelCliked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
#pragma mark
#pragma mark Search Functionality
-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [searchResults removeAllObjects];
    
    if (!searchText.length)
    {
        NSData *DataDonors=[SRUtility getdatafromplist:@"Donors" plist:@"Data"];
        NSDictionary *DicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:DataDonors];
        ArrTableData=[DicTmp objectForKey:@"data"];
        NSMutableDictionary *dictAddnewDonor=[[NSMutableDictionary alloc]init];
        [dictAddnewDonor setObject:@"+ Add New Donor" forKey:@"donor_name"];
        [ArrTableData insertObject:dictAddnewDonor atIndex:0];
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"donor_name" ascending:YES];
        [ArrTableData sortUsingDescriptors:[NSArray arrayWithObject:sort]];
        
        //[_TblSettings reloadData];
    }
    else
    {
        // Filter the array using NSPredicate
      
       
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.donor_name contains[c] %@",searchText];
            //searchResults=[check.];
        
        
       
        
        searchResults = [NSMutableArray arrayWithArray:[ArrTableData filteredArrayUsingPredicate:predicate]];
        ArrTableData=[searchResults mutableCopy];
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"donor_name" ascending:YES];
        [ArrTableData sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    }
    
        
        [TblDonorDetails reloadData];
 
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
}

#pragma mark - UISearchDisplayController Delegate Methods
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    //[[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor blackColor]];
   // [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil]setDefaultTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
    //[self positionForBar:0];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{   //appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //ArrData=[appDelegate getDonors];
    [self.searchBar resignFirstResponder];
    NSData *DataDonors=[SRUtility getdatafromplist:@"Donors" plist:@"Data"];
    NSDictionary *DicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:DataDonors];
    ArrTableData=[DicTmp objectForKey:@"data"];
    NSMutableDictionary *dictAddnewDonor=[[NSMutableDictionary alloc]init];
    [dictAddnewDonor setObject:@"+ Add New Donor" forKey:@"donor_name"];
    [ArrTableData insertObject:dictAddnewDonor atIndex:0];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"donor_name" ascending:YES];
    [ArrTableData sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    [TblDonorDetails reloadData];
    // [_TblSettings scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    //if we only try and resignFirstResponder on textField or searchBar,
    //the keyboard will not dissapear (at least not on iPad)!
    [self performSelector:@selector(searchBarCancelButtonClicked:) withObject:self.searchBar afterDelay: 0.1];
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    textField.placeholder=Nil;
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    
    
    return YES;
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    if (ObjDonorDetail.TxtEmailID)
    {
        [self.view setFrame:CGRectMake(0,-150,1024,714)];
        
    }
    else
    [self.view setFrame:CGRectMake(0,-435,1024,714)]; //here taken -20 for example i.e. your view will be scrolled to -20. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    
    [self.view setFrame:CGRectMake(0,0,1024,714)];
}


@end
