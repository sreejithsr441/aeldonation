//
//  DonorDetailsViewController.h
//  AREarlyLearningApp
//
//  Created by Sreejith Rajan on 09/03/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DonorInformationView.h"
#import "PopoverView.h"
#import "MBProgressHUD.h"

@protocol donorDelegate
@optional
-(void)recieveDonorDetails:(NSDictionary *)dictDonor;
-(void)PresentDonorSignUp;
-(void)SignUpData:(NSDictionary *)dictData;
@end

#import "AppDelegate.h"
@class AppDelegate;
@interface DonorDetailsViewController : UIViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,PopoverViewDelegate>
{   NSDictionary *dictCurrent;
    NSArray *ArrData;
    DonorInformationView *ObjDonorDetail;
    NSMutableArray *searchResults;
    AppDelegate *appDelegate;
    NSMutableArray *ArrTableData;
    PopoverView *pv;
    PopoverView *pv1;
    NSArray *Arr;
   NSMutableArray *array;
    MBProgressHUD *HUD;
    NSArray *ArrCenter;
    NSArray *ArrState;
    
}
@property(strong,nonatomic) UITableView *TblState;
@property(strong,nonatomic) UITableView *TblViewCenter;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *TblDonorDetails;
@property (weak, nonatomic) IBOutlet UIButton *BtnCancelClicked;
- (IBAction)BtnCancelCliked:(id)sender;
@property(nonatomic,retain) id<donorDelegate>DonorChange;
@end
