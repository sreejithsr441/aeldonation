//
//  DonationDetailedView.h
//  AREarlyLearningApp
//
//  Created by HIWORTH2 on 3/5/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRUtility.h"

@interface DonationDetailedView : UIView<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *ImgDonate;
@property (weak, nonatomic) IBOutlet UILabel *LblDonateDetails;
@property (weak, nonatomic) IBOutlet UILabel *LblQty;
@property (weak, nonatomic) IBOutlet UILabel *LblDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
@property (weak, nonatomic) IBOutlet UILabel *LblValue;
@property (weak, nonatomic) IBOutlet UIButton *btnMinus;
@property (weak, nonatomic) IBOutlet UITextView *TxtSplReqst;
@property (weak, nonatomic) IBOutlet UIButton *btnOverRidePriice;
@property (weak, nonatomic) IBOutlet UIButton *btnModifier;
@property (weak, nonatomic) IBOutlet UITextField *specialRequestTxt;
@property (weak, nonatomic) IBOutlet UIButton *SplRequestSave;

@end
