//
//  DonorSignUpViewController.m
//  AREarlyLearningApp
//
//  Created by Sabin.MS on 22/03/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import "DonorSignUpViewController.h"
#import "SRUtility.h"
#import "PopoverView.h"
#import "SRTableViewCell.h"
#import "AppDelegate.h"

@interface DonorSignUpViewController () {
    AppDelegate *delegate;
    DashBoardViewController *parentViewController;
}
@property (weak, nonatomic) IBOutlet UIPickerView *statesPicker;
@property (weak, nonatomic) IBOutlet UIButton *centreTappedBtn;

@end

@implementation DonorSignUpViewController
@synthesize DonorDelegate;
- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    parentViewController = delegate.dashBoardViewController;
    [[UITextField appearance] setTintColor:[UIColor whiteColor]];
    _statesPicker.hidden = YES;
    state_Array=[[NSArray alloc]initWithObjects:@"a",@"b", nil];
    flag=0;
    _TxtFrstNme.layer.borderWidth=1.0f;
    _TxtFrstNme.layer.borderColor=[UIColor whiteColor].CGColor;
    _TxtScndNme.layer.borderWidth=1.0f;
    _TxtScndNme.layer.borderColor=[UIColor whiteColor].CGColor;
    _TxtAddrOne.layer.borderColor=[UIColor whiteColor].CGColor;
    _TxtAddrOne.layer.borderWidth=1.0f;
    _TxtAddrTwo.layer.borderColor=[UIColor whiteColor].CGColor;
    _TxtAddrTwo.layer.borderWidth=1.0f;
    _TxtEmailAddr.layer.borderColor=[UIColor whiteColor].CGColor;
    _TxtEmailAddr.layer.borderWidth=1.0f;
    _TxtCell.layer.borderColor=[UIColor whiteColor].CGColor;
    _TxtCell.layer.borderWidth=1.0f;
    _TxtPhne.layer.borderColor=[UIColor whiteColor].CGColor;
    _TxtPhne.layer.borderWidth=1.0f;
    _TxtCity.layer.borderWidth=1.0f;
    _TxtCity.layer.borderColor=[UIColor whiteColor].CGColor;
    _TxtPin.layer.borderWidth=1.0f;
    _TxtPin.layer.borderColor=[UIColor whiteColor].CGColor;
    _TxtStte.layer.borderWidth=1.0f;
    _TxtStte.layer.borderColor=[UIColor whiteColor].CGColor;
    _TxtCenter.layer.borderWidth=1.0f;
    _TxtCenter.layer.borderColor=[UIColor whiteColor].CGColor;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    internetReachable = [Reachability reachabilityForInternetConnection] ;
    [internetReachable startNotifier];
    [self setUpRechability];
    NSData *statesData=[SRUtility getdatafromplist:@"PlacesCentre" plist:@"Data"];
    NSDictionary *stateDictionary=[NSKeyedUnarchiver unarchiveObjectWithData:statesData];
    state_Array=[stateDictionary objectForKey:@"data"];
    NSData *centerData=[SRUtility getdatafromplist:@"PlacesCentre2" plist:@"Data"];
    NSDictionary *centerDictionary=[NSKeyedUnarchiver unarchiveObjectWithData:centerData];
    ArrCenter=[centerDictionary objectForKey:@"data"];
    NSData *cityData=[SRUtility getdatafromplist:@"CityNames" plist:@"Data"];
    NSDictionary *cityDictionary=[NSKeyedUnarchiver unarchiveObjectWithData:cityData];
    city_Array=[cityDictionary objectForKey:@"data"];
    // Do any additional setup after loading the view.
}
-(void)setUpRechability
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNetworkChange:) name:kReachabilityChangedNotification object:nil];
    
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if          (remoteHostStatus == NotReachable)      {NSLog(@"no");      self.hasInet-=NO;   }
    else if     (remoteHostStatus == ReachableViaWiFi)  {NSLog(@"wifi");    self.hasInet-=YES;  }
    else if     (remoteHostStatus == ReachableViaWWAN)  {NSLog(@"cell");    self.hasInet-=YES;  }
    
}

- (void) handleNetworkChange:(NSNotification *)notice
{
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if          (remoteHostStatus == NotReachable)      {NSLog(@"no");      self.hasInet-=NO;   }
    else if     (remoteHostStatus == ReachableViaWiFi)  {NSLog(@"wifi");    self.hasInet-=YES;  }
    else if     (remoteHostStatus == ReachableViaWWAN)  {NSLog(@"cell");    self.hasInet-=YES;  }
    
    //    if (self.hasInet) {
    //        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Net avail" message:@"" delegate:self cancelButtonTitle:OK_EN otherButtonTitles:nil, nil];
    //        [alert show];
    //    }
}
- (void)checkNetworkStatus:(NSNotification *)notice {
    // called after network status changes
    
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            NSLog(@"The internet is down.");
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Lost Network Connection" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"The internet is working via WIFI");
            //[self MakeWebCallWhenConnectedToNet];
            //[_LblTransaction setText:@""];
            
            
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"The internet is working via WWAN!");
            //[self MakeWebCallWhenConnectedToNet];
            // [_LblTransaction setText:@""];
            
            
            
            break;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{  NSUInteger RowCount;
    switch (tableView.tag)
    {
        case 0:
            {
                RowCount=[state_Array count];
            }
            break;
        case 1:
            {
                RowCount=[ArrCenter count];
            }
            break;
            case 2:
           {
               RowCount=[city_Array count];
           }
            break;
        default:
            break;
    }
  
   
    return RowCount;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SRTableViewCell *cell=(SRTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"myidentifier"];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SRTableViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    cell.LblDollTwo.hidden=YES;
    cell.btnAdd.hidden=YES;
    cell.btnMinus.hidden=YES;
    cell.DonorName.hidden=NO;
    cell.LblDataDolltwo.hidden=YES;
    if (tableView.tag==0)
    {
       // [cell.DonorName setText:[state_Array objectAtIndex:indexPath.row]];
             //  NSString *key = [stateDictionary allKeys][indexPath.row];
        
        //NSString *providerNameString = stateDictionary[key];
        //NSString *providerIdString = key;
        cell.LblTblDetails.text  = [[state_Array objectAtIndex:indexPath.row] objectForKey:@"state"];
        //cell.detailTextLabel.text  = providerIdString;
        
    }
    if (tableView.tag==1)
    {
        
        cell.LblTblDetails.text=[[ArrCenter objectAtIndex:indexPath.row] objectForKey:@"centre"];
        cell.LblTblDetails.lineBreakMode=UILineBreakModeWordWrap;
        cell.LblTblDetails.numberOfLines=3;
    }
    if (tableView.tag==2)
    {
        cell.LblTblDetails.text=[[city_Array objectAtIndex:indexPath.row ]objectForKey:@"city"];
        //cell.LblTblDetails.numberOfLines=3;
        
    }
    return cell;
    
}
-(void)OfflineSignup
{
    NSString *StrToPass=[SRUtility getfromplist:@"OfflinDonor" plist:@"Data"];
    if ([StrToPass length])
    {
        
        
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag==0||tableView.tag==1||tableView.tag==2)
    {
        
        [self popoverView:pv didSelectItemAtIndex:indexPath.row];
    }
    
}
- (IBAction)BtnBackClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:Nil];
    if (self.DonorDelegate)
        [DonorDelegate PresentDonors];
        
    
    
}
- (IBAction)BtnCancelClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:Nil];
    if (self.DonorDelegate)
        [DonorDelegate PresentDonors];
        
    
}
- (IBAction)BtnApplyClicked:(id)sender
{
    bool pass=true;
    if ([_TxtFrstNme.text isEqualToString:@""]) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"First name is missing" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
        [alert show];
        pass=false;
    }
    if (pass) {
        if ([_TxtScndNme.text isEqualToString:@""]) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Second name is missing" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
            [alert show];
            pass=false;
            
        }
    }
    if (pass) {
        NSLog(@"%@",_centreDrop.currentTitle);
        if (!_centreDrop.currentTitle.length) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please add a Center" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
            [alert show];
            pass=false;
            
        }
    }
    if (pass) {
        if (!_btnCityDrop.currentTitle.length) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please add a City" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
            [alert show];
            pass=false;
        }
    }
    
/*

   // if (pass) {
       //if ([_TxtAddrOne.text isEqualToString:@""]) {
            //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Address1 Field is missing" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
            //[alert show];
         //   _TxtAddrOne.text=@" ";
         //   pass=false;
            
       // }
   // }
    if (pass) {
        if ([_TxtAddrTwo.text isEqualToString:@""]) {
            //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Address2 Field is missing" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
            //[alert show];
            _TxtAddrTwo.text=@" ";
            pass=false;
            
        }
    }
    if (pass) {
        if ([_TxtEmailAddr.text isEqualToString:@""]) {
            //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Email id is missing" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
            //[alert show];
            _TxtEmailAddr.text=@" ";
            pass=false;
            
        }
    }
    if (pass) {
        if ([_centreDrop.titleLabel.text isEqualToString:@""]) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please add a Center" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
            [alert show];
            pass=false;
            
        }
    }
    if (pass) {
        if ([_TxtPhne.text isEqualToString:@""]) {
            //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please add phone Number" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
            //[alert show];
            _TxtPhne.text=@" ";
            pass=false;
            
        }
    }
    if (pass) {
        if ([_TxtCell.text isEqualToString:@""]) {
            //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please add Cell Number" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
           // [alert show];
            _TxtCell.text=@" ";
            pass=false;
            
        }
    }
    if (pass) {
        if ([_TxtCity.text isEqualToString:@""]) {
           // UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please input your city" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
           // [alert show];
            _TxtCity.text=@" ";
            pass=false;
            
        }
    }
    if (pass) {
        if ([_TxtStte.text isEqualToString:@""]) {
            //UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please input your state" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
            //[alert show];
            _TxtStte.text=@" ";
            pass=true;
            
        }
    }
    if (pass) {
        if ([_TxtPin.text isEqualToString:@""]) {
           // UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please input your Zip Code" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
            //[alert show];
            _TxtPin.text=@" ";
            pass=false;
            
        }
    }
    if (pass) {
        if ([_TxtNotes.text isEqualToString:@""]) {
          _TxtNotes.text=@" ";
            pass=true;
            
        }
    }
 */

    if (pass) {
        
    
    
    NSData *DataDonors=[SRUtility getdatafromplist:@"Donors" plist:@"Data"];
    NSDictionary *DicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:DataDonors];
    NSMutableArray *ArrDonors=[DicTmp objectForKey:@"data"];
    NSMutableDictionary *dictDonor=[[NSMutableDictionary alloc]init];
    [dictDonor setObject:_TxtAddrOne.text forKey:@"address"];
   // [dictDonor setObject:_TxtCity.text forKey:@"city"];
    NSString *StrDonFNme=[NSString stringWithFormat:@"%@ ",_TxtFrstNme.text];
    NSString *StrFullNme=[StrDonFNme stringByAppendingString:_TxtScndNme.text];
    [dictDonor setObject:StrFullNme forKey:@"donor_name"];
    [dictDonor setObject:_TxtEmailAddr.text forKey:@"email"];
    [dictDonor setObject:_TxtPhne.text forKey:@"phone1"];
    [dictDonor setObject:_TxtCell.text forKey:@"phone2"];
    //[dictDonor setObject:_TxtStte.text forKey:@"state"];
    [dictDonor setObject:_TxtPin.text forKey:@"zip"];
        [dictDonor setObject:[_centreDrop titleForState:UIControlStateNormal] forKey:@"centre"];
        [dictDonor setObject:[_btnState titleForState:UIControlStateNormal] forKey:@"state"];
        [dictDonor setObject:[_btnCityDrop titleForState:UIControlStateNormal] forKey:@"city"];

        
        
        
    NSError *error;
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dictDonor options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
   // NSString *StrValue=[NSString stringWithFormat:@"data=%@",jsonString];
    NSString *StringToPass=[jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
//   NSDictionary* dicWebResponse=[SRUtility makeWebServiceCallForPOSTMethod:[NSURL URLWithString:[NSString stringWithFormat:@"http://aceonetest.com/webservice/createdonor.php"]] withAppendData:StringToPass withRequestHeaderOne:@"User-Agent" withRequestHeaderTwo:@"Content-Type" withHeaderValueOne:@"ASIHTTPRequest" withHeaderValueTwo:@"application/json"];
//     NSMutableArray *donorAdded=dicWebResponse[@"data"];
//    NSMutableDictionary *newDonor=[donorAdded objectAtIndex:0];
 //   [ArrDonors addObject:newDonor];
//         
        if ([SRUtility reachable])
        {

      __block NSDictionary *dicWebResponse;
       HUD=[[MBProgressHUD alloc]initWithView:self.view];
      [self.view addSubview:HUD];
      [HUD showAnimated:YES whileExecutingBlock:^(void)
       {
         dicWebResponse=[SRUtility makeWebServiceCallForPOSTMethod:[NSURL URLWithString:[NSString stringWithFormat:@"http://donations.arearlylearning.org/donation-api-test/createdonor.php"]] withAppendData:StringToPass withRequestHeaderOne:@"User-Agent" withRequestHeaderTwo:@"Content-Type" withHeaderValueOne:@"ASIHTTPRequest" withHeaderValueTwo:@"application/json"];
       }
      completionBlock:^(void)
     {
         if ([dicWebResponse objectForKey:@"status_message"])
         {
             parentViewController.offlineUserSignedUp = NO;
             //[[NSNotificationCenter defaultCenter]postNotificationName:@"ReloadSignup" object:dicWebResponse];
//             if (self.DonorDelegate)
//             {
//                 //[DonorDelegate recieveDonorDetails:[[dicWebResponse objectForKey:@"data"] objectAtIndex:0]];
//                 [DonorDelegate SignUpData:[[dicWebResponse objectForKey:@"data"] objectAtIndex:0]];
//             }
               [ArrDonors addObject:[[dicWebResponse objectForKey:@"data"] objectAtIndex:0]];
             NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"donor_name" ascending:YES];
             [ArrDonors sortUsingDescriptors:[NSArray arrayWithObject:sort]];
               NSDictionary *dictData=[[NSDictionary alloc] initWithObjectsAndKeys:ArrDonors, @"data",nil];
                NSData *dataToSave=[NSKeyedArchiver archivedDataWithRootObject:dictData];
                [SRUtility adddatatoplist:dataToSave key:@"Donors" plist:@"Data"];
                [self dismissViewControllerAnimated:YES completion:Nil];

             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Donor Successfully Added" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [alert show];
            [self dismissViewControllerAnimated:YES completion:Nil];
             if (self.DonorDelegate)
             {
                 [DonorDelegate SignUpData:dicWebResponse];
             }
             

         }
     }
         ];
    
        }
        else
        {
            [SRUtility addtoplist:StringToPass key:@"OfflinDonor" plist:@"Data"];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"No network connection.  User will be added when network connection is restored." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
             [self dismissViewControllerAnimated:YES completion:Nil];
            
        }
//    NSDictionary *dictData=[[NSDictionary alloc] initWithObjectsAndKeys:ArrDonors, @"data",nil];
//    NSData *dataToSave=[NSKeyedArchiver archivedDataWithRootObject:dictData];
//    [SRUtility adddatatoplist:dataToSave key:@"Donors" plist:@"Data"];
//    [self dismissViewControllerAnimated:YES completion:Nil];
//
    }
     
}
- (IBAction)centreDropDown:(id)sender {
    
     [self.view endEditing:YES];
    [self performSelector:@selector(keyboardDidHide:) withObject:nil afterDelay:0.3];
    //CGPoint Point=_centreDrop.center;
   // Array=[NSArray arrayWithObjects:@"Virginia",@"Texas",@"Florida",nil];
   // pv=[PopoverView showPopoverAtPoint:Point inView:self.view withContentView:TblViewUserPopUp delegate:self];
    
    CGPoint pointOfcenter=_centreDrop.center;
    _TblViewCenter=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 250, 120)];
    _TblViewCenter.tag=1;
    _TblViewCenter.delegate=self;
    _TblViewCenter.dataSource = self;
    _TblViewCenter.scrollEnabled=YES;
    _TblViewCenter.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    pv=[PopoverView showPopoverAtPoint:pointOfcenter inView:self.view withContentView:_TblViewCenter delegate:self];
   // pv.tag=200;
  //  pv=[PopoverView showPopoverAtPoint:Point inView:self.view withStringArray:Array delegate:self];
    pv.tag=200;
}

- (IBAction)cityDropDown:(id)sender {
    
    [self.view endEditing:YES];
    [self performSelector:@selector(keyboardDidHide:) withObject:nil afterDelay:0.3];
    //CGPoint Point=_centreDrop.center;
    // Array=[NSArray arrayWithObjects:@"Virginia",@"Texas",@"Florida",nil];
    // pv=[PopoverView showPopoverAtPoint:Point inView:self.view withContentView:TblViewUserPopUp delegate:self];
    
    CGPoint pointOfcenter=_btnCityDrop.center;
    _TblCity=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 250, 120)];
    _TblCity.tag=2;
    _TblCity.delegate=self;
    _TblCity.dataSource = self;
    _TblCity.scrollEnabled=YES;
    _TblCity.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    pv=[PopoverView showPopoverAtPoint:pointOfcenter inView:self.view withContentView:_TblCity delegate:self];
    // pv.tag=200;
    //  pv=[PopoverView showPopoverAtPoint:Point inView:self.view withStringArray:Array delegate:self];
    pv.tag=300;

}



-(void)popoverView:(PopoverView *)popoverView didSelectItemAtIndex:(NSInteger)index
{
    
    [popoverView performSelector:@selector(dismiss) withObject:nil afterDelay:0.3];
    if (pv.tag==200)
    {
         [_centreDrop setTitle:[[ArrCenter objectAtIndex:index] objectForKey:@"centre"] forState:UIControlStateNormal];
    }
    if (pv.tag==100)
    {
        [_btnState setTitle:[[state_Array objectAtIndex:index] objectForKey:@"state"] forState:UIControlStateNormal];
    }
    if (pv.tag==300)
    {
        [_btnCityDrop setTitle:[[city_Array objectAtIndex:index]objectForKey:@"city"] forState:UIControlStateNormal];
    }
    //btnCntry.text=[[Array objectAtIndex:index];
}
- (IBAction)stateTapped:(id)sender
{   [self.view endEditing:YES];
    [self performSelector:@selector(keyboardDidHide:) withObject:nil afterDelay:0.3];
   //_statesPicker.hidden=NO;
  //  NSData *statesData=[SRUtility getdatafromplist:@"PlacesCentre" plist:@"Data"];
   // NSDictionary *stateDictionary=[NSKeyedUnarchiver unarchiveObjectWithData:statesData];
    CGPoint point=_btnState.center;
    //state_Array=[[NSArray alloc] initWithObjects:@"A",@"B",@"C",@"D", nil];
    _TblViewState=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 250, 150)];
    _TblViewState.tag=0;
    _TblViewState.delegate=self;
    _TblViewState.dataSource = self;
    _TblViewState.scrollEnabled=YES;
    _TblViewState.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    pv=[PopoverView showPopoverAtPoint:point inView:self.view withContentView:_TblViewState delegate:self];
    pv.tag=100;
    
//    NSData *centerData=[SRUtility getdatafromplist:@"PlacesCentre2" plist:@"Data"];
//    NSDictionary *centerDictionary=[NSKeyedUnarchiver unarchiveObjectWithData:centerData];
//    CGPoint pointOfcenter=_centreDrop.center;
//    _TblViewState=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 250, 120)];
//    _TblViewState.tag=3;
//    _TblViewState.delegate=self;
//    _TblViewState.dataSource = self;
//    _TblViewState.scrollEnabled=YES;
//    _TblViewState.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
//    pv=[PopoverView showPopoverAtPoint:pointOfcenter inView:self.view withContentView:_TblViewState delegate:self];
//    pv.tag=200;
    
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    //NSLog(@"Number of rows: %d", [_genderArray count]);
    return [state_Array count];
}
-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width/2, 162.0)];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor darkTextColor];
    label.text = [state_Array objectAtIndex:row];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:16];
    
    return label;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    [_centreTappedBtn setTitle:[state_Array objectAtIndex:row] forState:UIControlStateNormal];
    [_centreTappedBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _statesPicker.hidden = YES;
    
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-130,1024,714)]; //here taken -20 for example i.e. your view will be scrolled to -20. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
   
    [self.view setFrame:CGRectMake(0,0,1024,714)];
}

@end
