//
//  SRDailyReprtsTableViewCell.h
//  AREarlyLearningApp
//
//  Created by Sabin.MS on 15/04/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRDailyReprtsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *LblTrnsction;
@property (weak, nonatomic) IBOutlet UILabel *LblNmer;
@property (weak, nonatomic) IBOutlet UILabel *LblDntions;
@property (weak, nonatomic) IBOutlet UIImageView *ImgStatus;

@end
