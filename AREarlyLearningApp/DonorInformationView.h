//
//  DonorInformationView.h
//  AREarlyLearningApp
//
//  Created by Sreejith Rajan on 09/03/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DonorInformationView : UIView
@property (weak, nonatomic) IBOutlet UILabel *LblName;
@property (weak, nonatomic) IBOutlet UILabel *LblDonatedDate;
@property (weak, nonatomic) IBOutlet UILabel *LblStreet;
@property (weak, nonatomic) IBOutlet UILabel *LblState;
@property (weak, nonatomic) IBOutlet UILabel *LblPin;
@property (weak, nonatomic) IBOutlet UILabel *LblCntry;
@property (weak, nonatomic) IBOutlet UILabel *LblEmailID;

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnDonorSelect;
@property (strong, nonatomic) IBOutlet UIButton *btnDonatedDate;
@property (strong, nonatomic) IBOutlet UIButton *btnStreet;
@property (weak, nonatomic) IBOutlet UIButton *BtnCenter;

@property (strong, nonatomic) IBOutlet UIButton *btnState;
@property (strong, nonatomic) IBOutlet UIButton *btnPin;
@property (strong, nonatomic) IBOutlet UIButton *btnCntry;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UITextField *Txtname;
@property (strong, nonatomic) IBOutlet UITextField *TxtCellNum;
@property (weak, nonatomic) IBOutlet UITextField *TxtEmailID;

@property (weak, nonatomic) IBOutlet UILabel *LblCellNum;
@property (strong, nonatomic) IBOutlet UIImageView *ImgBacKGRnd;
@property (strong, nonatomic) IBOutlet UITextField *editDonatedDate;
@property (strong, nonatomic) IBOutlet UITextField *editStreet;
@property (strong, nonatomic) IBOutlet UITextField *editPin;
@property (weak, nonatomic) IBOutlet UILabel *LblCenter;
@property (weak, nonatomic) IBOutlet UILabel *LblNameCenter;


@end
