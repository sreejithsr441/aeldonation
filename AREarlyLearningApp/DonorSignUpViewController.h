//
//  DonorSignUpViewController.h
//  AREarlyLearningApp
//
//  Created by Sabin.MS on 22/03/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "PopoverView.h"
#import "Reachability.h"

@protocol donors
@optional
-(void)PresentDonors;
-(void)SignUpData:(NSDictionary *)dictData;
-(void)recieveDonorDetails:(NSDictionary *)dictDonor;

@end

#import "DashBoardViewcontroller.h"

@interface DonorSignUpViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,PopoverViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITableViewDataSource,UITableViewDelegate>
{
    MBProgressHUD *HUD;
    int flag;
    PopoverView *pv;
    NSArray *Array;
    NSArray *state_Array;
    Reachability *internetReachable;
    Reachability *reachability;
    NSArray *ArrCenter;
    NSArray *city_Array;
    NSArray *ArrCity;
    
}
@property (nonatomic, assign) BOOL hasInet;
@property (strong, nonatomic) IBOutlet UITextField *TxtCenter;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
- (IBAction)BtnBackClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *TxtFrstNme;
@property (strong, nonatomic) IBOutlet UITextField *TxtScndNme;
@property (strong, nonatomic) IBOutlet UITextField *TxtAddrOne;
@property (strong, nonatomic) IBOutlet UITextField *TxtAddrTwo;
@property (strong, nonatomic) IBOutlet UITextField *TxtEmailAddr;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
- (IBAction)BtnCancelClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *TxtPhne;
@property (strong, nonatomic) IBOutlet UITextField *TxtCell;
@property (strong, nonatomic) IBOutlet UITextField *TxtCity;
@property (strong, nonatomic) IBOutlet UITextField *TxtStte;
@property (strong, nonatomic) IBOutlet UITextField *TxtPin;
@property (strong, nonatomic) IBOutlet UITextView *TxtNotes;
@property (strong, nonatomic) IBOutlet UIButton *btnApple;
- (IBAction)BtnApplyClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *centreDrop;
@property (weak, nonatomic) IBOutlet UIButton *btnState;
@property(nonatomic,retain) id<donors>DonorDelegate;
@property(strong,nonatomic) UITableView *TblViewState;
@property(strong,nonatomic) UITableView *TblViewCenter;
@property(strong,nonatomic) UITableView *TblCity;

@property (weak, nonatomic) IBOutlet UIButton *btnCityDrop;

@end
