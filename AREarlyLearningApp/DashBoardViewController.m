
//
//  DashBoardViewController.m
//  AREarlyLearningApp
//
//  Created by Sreejith Rajan on 02/03/15.
//  Copyright (c) 2015 Sreejith Rajan. All rights reserved.
//

#import "DashBoardViewController.h"
#import "BFPaperButton.h"
#import "UIColor+BFPaperColors.h"
#import "SRTableViewCell.h"
#import "SRUtility.h"
#import "SRCollectionViewCell.h"
#import "Product_Export_Establishment.h"
#import "SRDonorTableViewCell.h"
#import "EmailViewController.h"
#import "SignatureViewController.h"
#import "In_kind_volunteers.h"
#import "DonorDetailsViewController.h"
#import "NSString+Date.h"
#import "DonorSignUpViewController.h"
#import "DailyReportsViewController.h"
#import "ChangePassViewController.h"
#import "Reachability.h"
#import "PassWordViewController.h"
#import "BasciHistoryObject.h"
#import "AFNetworkReachabilityManager.h"
#import "AppDelegate.h"
@interface DashBoardViewController ()
{
    BOOL reachabilityShown;
    BOOL webCallAfterNotConnectedMade;
    BOOL currentlyAddingDonationItem;
    AppDelegate *delegate;
}

@end
@implementation DashBoardViewController
@synthesize TblViewDonation,TblViewUserPopUp, offlineUserSignedUp, transactionCompletedShown;
- (void)viewDidLoad
{
    NSLog(@"view did load");
    reachabilityShown = NO;
    savedTransaction= NO;
    offlineUserSignedUp = NO;
    transactionCompletedShown = NO;
    webCallAfterNotConnectedMade = NO;
    currentlyAddingDonationItem = NO;
    [super viewDidLoad];
    [self InitView];
    TotalValueSum=0;
    __block NSDictionary *transaction;
    if ([SRUtility reachable])
    {
        NSData *data=[SRUtility getdatafromplist:@"Donations" plist:@"Data"];
        NSUInteger DataLength=[data length];
        
        if (!DataLength==0) [self MakeWebCallWhenConnectedToNet];
        
        
        HUD=[[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:HUD];
        [HUD showAnimated:YES whileExecutingBlock:^(void)
         {
             transaction=[SRUtility makeWebServicecallForGetmethod:[NSURL URLWithString:[NSString stringWithFormat:@"http://donations.arearlylearning.org/donation-api-test/index.php?request=transaction"]]];
         } completionBlock:^(void)
         {
            NSDecimalNumber *transactionID=transaction[@"data"];
            stringOFTransaction=[NSString stringWithFormat:@"%@",transactionID];
             if([stringOFTransaction isEqualToString:@"(null)"]) {
                 _labltrasactionId.text=@"";
             }
             else
             [_labltrasactionId setText:stringOFTransaction];
         }];
    }
    else
    {
        if(!reachabilityShown)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Not connected to the internet." delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [_labltrasactionId setText:@""];
            reachabilityShown = YES;
        }
        
    }
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        offlineUserSignedUp = NO;
        transactionCompletedShown = NO;
        if([SRUtility reachable])
        {
            [self MakeWebCallWhenConnectedToNet];
        }
        [self OfflineUserSignup];

        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
    }];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    [prefs setObject:@"nothing" forKey:@"OverRideViewController"];
    [prefs synchronize];
    NSString *StrFnme=[NSString stringWithFormat:@"%@",[[[_dicUserDeatils objectForKey:@"data"] objectAtIndex:0] objectForKey:@"user_fname"]];
    NSUserDefaults *prefsDataDonors=[NSUserDefaults standardUserDefaults];
    [prefsDataDonors setObject:@"FromDonationView" forKey:@"DonorSelectionTable"];
    [prefsDataDonors synchronize];
    NSString *StrFullNme=[StrFnme stringByAppendingString:[NSString stringWithFormat:@" %@",[[[_dicUserDeatils objectForKey:@"data"] objectAtIndex:0] objectForKey:@"user_lname"]]];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(PresentDonorSignUp) name:@"DonorSignUpViewController" object:nil];
    [_btnUserNme setTitle:StrFullNme forState:UIControlStateNormal];
    _btnUserNme.titleLabel.numberOfLines=3;
   NSData *data=[SRUtility getdatafromplist:@"Donations" plist:@"Data"];
    ArrOfflineData=[NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (![data length]||[ArrOfflineData count]==0)
    {
        ArrOfflineData=[[NSMutableArray alloc]init];
        
    }
    ArrCollData=[[NSMutableArray alloc]init];
    [_btnback setHidden:YES];
    [_testButton setHidden:YES];
    [_itemNavigationBtn setHidden:YES];
    [_LblDonorName setHidden:YES];
    ObjDonDetailedView=[[[NSBundle mainBundle]loadNibNamed:@"DonationDetailedView" owner:self options:nil ] objectAtIndex:0];
    [ObjDonDetailedView.btnModifier addTarget:self action:@selector(BtnModifierClicked:) forControlEvents:UIControlEventTouchUpInside];
    [ObjDonDetailedView.btnAdd addTarget:self action:@selector(DonationIncrement:) forControlEvents:UIControlEventTouchUpInside];
    [ObjDonDetailedView.btnMinus addTarget:self action:@selector(DonationIncrement:) forControlEvents:UIControlEventTouchUpInside];
    [ObjDonDetailedView.btnOverRidePriice addTarget:self action:@selector(OverRidePricePopUp:) forControlEvents:UIControlEventTouchUpInside];
    [ObjDonDetailedView.SplRequestSave addTarget:self action:@selector(specialRequestSaved:) forControlEvents:UIControlEventTouchUpInside];
    ObjDonDetailedView.TxtSplReqst.delegate=self;
    ObjDonDetailedView.specialRequestTxt.delegate=self;
    [self colleInData];
    self.TblViewDonation.allowsMultipleSelectionDuringEditing = NO;
     ArrTableData=[[NSMutableArray alloc]init];
     UIImage *ImgProPic=[self loadImage];
     if (ImgProPic)
       [self InitProPic];
     else
       [_btnProPic setBackgroundImage:[UIImage imageNamed:@"anon_user.png"] forState:UIControlStateNormal];
     Valuesub=1;
     TblViewDonation .tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _searchBar.barTintColor=[UIColor whiteColor];
    _searchBar.backgroundColor=[UIColor clearColor];
    _searchBar.barStyle=UISearchBarStyleProminent;
    TblViewDonation.layer.borderColor=[UIColor grayColor].CGColor;
    TblViewDonation.layer.borderWidth=1.0;
    TblViewDonation.layer.cornerRadius=8.0;
       [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(COllectionChangeItem) name:@"CollectionReload"object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(TableReload) name:@"Reloadtable" object:Nil];
    NSDate *Now=[NSDate date];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ChangeUserName) name:@"UserNameChanged" object:Nil];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MMMM-dd-EEEE HH:mm:ss"];
    NSString *StrDate=[dateFormatter stringFromDate:Now];
    NSLog(@"%@",StrDate);
    //[self setUpRechability];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    //Do any additional setup after loading the view.
    NSLog(@"ViewDidLoad");
}

-(void)viewDidAppear:(BOOL)animated{
    HUD=[[MBProgressHUD alloc]initWithView:self.view];
    NSLog(@"viewDidAppear");
    [super viewDidAppear:YES];
    [self currentTime];
    
}

-(IBAction)BtnModifierClicked:(id)sender
{
    NSLog(@"BtnModifierClicked");
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    [prefs setObject:@"ModifyQuantity" forKey:@"OverRideViewController"];
    [prefs synchronize];
    FormSheetViewController *ObjFormSheet=[self.storyboard instantiateViewControllerWithIdentifier:@"FormSheetViewController"];
    ObjFormSheet.dicItem=[ArrTableData objectAtIndex:[sender tag]];
    ObjFormSheet.categoryChange=self;
    NSString *StrCellTag=[NSString stringWithFormat:@"%ld",(long)[sender tag]];
    ObjFormSheet.CellTag=StrCellTag;
    [self.parentViewController presentViewController: ObjFormSheet animated:YES completion:nil];
    
    
}

#pragma syncDataToServer
-(void)currentTime
{
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"hh:mm:ss"];
    [self performSelector:@selector(currentTime) withObject:self afterDelay:1.0];
    NSString *syncingTime=[dateFormat stringFromDate:[NSDate date]];
    if ([syncingTime isEqualToString:@"06:05:00"]||[syncingTime isEqualToString:@"04:00:00"]||[syncingTime isEqualToString:@"06:00:00"]||[syncingTime isEqualToString:@"08:00:00"]||[syncingTime isEqualToString:@"12:00:00"])
    {
        [self performSelector:@selector(targetMethod:) withObject:Nil];
    }
    
}

-(void)targetMethod:(id)sender
{
    NSLog(@"targetMethod");
    __block NSDictionary *dicWebResponse;
    HUD=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.labelText=@"Please Wait Loading Master data";
    [HUD showAnimated:YES whileExecutingBlock:^(void)
     {
         NSString *Strurl=@"http://donations.arearlylearning.org/donation-api-test/index.php?request=donors";
         NSURL *TheUrl=[NSURL URLWithString:Strurl];
         dicWebResponse=[SRUtility makeWebServicecallForGetmethod:TheUrl];
         
     }
      completionBlock:^(void)
     {
         if ([[dicWebResponse objectForKey:@"status_message"] isEqualToString:@"success"])
         {
             NSData *dataDonors=[NSKeyedArchiver archivedDataWithRootObject:dicWebResponse];
             [SRUtility adddatatoplist:dataDonors key:@"Donors" plist:@"Data"];
             
         }
         
     }];
    
    
}

-(void)MakeWebCallWhenConnectedToNet
{
    NSLog(@"MakeWebCallWhenConnectedToNet");
    NSData *data=[SRUtility getdatafromplist:@"Donations" plist:@"Data"];
   __block NSMutableArray *ArrToSave=[NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSData *DataOffline=[SRUtility getdatafromplist:@"OfflineTransaction" plist:@"Data"];
   __block NSMutableArray*ArrForSavingDataLocal=[NSKeyedUnarchiver unarchiveObjectWithData:DataOffline];
    __block int count = 1;
    int total = [ArrToSave count];
    NSMutableArray *time_array = [NSMutableArray new];
    for ( NSArray *ArrTmp in [ArrToSave reverseObjectEnumerator]) {
        NSString *signature_string = (NSString *)[[[[ArrTmp objectAtIndex:0] objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"donor_signature"];
        // quick fix for duplicates.  All duplicates were objects with empty signatures.
        if([signature_string length])
        {
            NSString *StrDateAndTime=[[[[ArrTmp objectAtIndex:0] objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"date_time"];
            [time_array addObject:StrDateAndTime];
            NSError *error;
            NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:ArrTmp options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
           // NSString *StrValue=[NSString stringWithFormat:@"data=%@",jsonString];
            __block NSMutableDictionary *dicWebRseponse;
            NSString *StringToPass=[jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];
            HUD=[[MBProgressHUD alloc]initWithView:self.view];
            HUD.labelText=@"Synchronising data with server.";
            
            [self.view addSubview:HUD];
            [HUD showAnimated:YES whileExecutingBlock:^(void)
            {
                 dicWebRseponse= [[SRUtility makeWebServiceCallForPOSTMethod:[NSURL URLWithString:[NSString stringWithFormat:@"http://donations.arearlylearning.org/donation-api/transaction.php"]] withAppendData:StringToPass withRequestHeaderOne:@"User-Agent" withRequestHeaderTwo:@"Content-Type" withHeaderValueOne:@"ASIHTTPRequest" withHeaderValueTwo:@"application/json"] mutableCopy];
            } completionBlock:^(void)
            {
                NSLog(@"[dicWebRseponse objectForKey:@\"status_message\"] : %@", [dicWebRseponse objectForKey:@"status_message"]);
                
                if ([dicWebRseponse objectForKey:@"status_message"]) {
                    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
                    [prefs setObject:@"FromDonationView" forKey:@"DonorSelectionTable"];
                    [prefs synchronize];
                    for (int i=0; i<[ArrForSavingDataLocal count]; i++)
                    {
                        for(NSString *date_time in time_array)
                        {
                            if ([[[[[ArrForSavingDataLocal objectAtIndex:i] objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"date_time"] isEqualToString:date_time] && [[[[[[dicWebRseponse objectForKey:@"data"] objectAtIndex:0] objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"date_time"] isEqualToString:date_time]) {
                                if ([[[dicWebRseponse objectForKey:@"data"] objectAtIndex:0] objectForKey:@"server_recieved"])
                                {
                                    [[[dicWebRseponse objectForKey:@"data"] objectAtIndex:0] removeObjectForKey:@"server_recieved"];
                                    [ArrForSavingDataLocal removeObjectAtIndex:i];
                                    [[[dicWebRseponse objectForKey:@"data"] objectAtIndex:0] setObject:@"1" forKey:@"server_recieved"];
                                    [ArrForSavingDataLocal addObject:[[dicWebRseponse objectForKey:@"data"] objectAtIndex:0]];
                                }
                                
                            }
                        }
                    }
                }
                
                NSLog(@"count : %d", total);
                NSLog(@"total : %d", count);
                if(count == total)
                {
                    
                    NSData *dataDonations=[NSKeyedArchiver archivedDataWithRootObject:ArrForSavingDataLocal];
                    [SRUtility adddatatoplist:dataDonations key:@"OfflineTransaction" plist:@"Data"];
                    
                    if(!transactionCompletedShown)
                    {
                        
                        // give everything a little buffer time to finish
                        dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 1.00);
                        dispatch_after(delay, dispatch_get_main_queue(), ^(void)
                        {
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"CollectionReload" object:nil];
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"Reloadtable" object:nil];
                        });
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Donation" message:@"Transaction successfully completed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alert show];
                        transactionCompletedShown = YES;
                    }
                    //  NSDictionary *dic=[dicWebRseponse mutableCopy];
                    ArrToSave = [NSMutableArray new];
                    NSData *data=[NSKeyedArchiver archivedDataWithRootObject:ArrToSave];
                    [SRUtility adddatatoplist:data key:@"Donations" plist:@"Data"];
                }
                count++;
             }];
        }
    }
}

-(void)OfflineUserSignup
{
    NSLog(@"OfflineUserSignup");
    NSString *StrToPass=[SRUtility getfromplist:@"OfflinDonor" plist:@"Data"];
    if ([StrToPass length])
    {
        NSData *DataDonors=[SRUtility getdatafromplist:@"Donors" plist:@"Data"];
        NSDictionary *DicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:DataDonors];
        NSMutableArray *ArrDonors=[DicTmp objectForKey:@"data"];
     //   NSPredicate *ThePredicate=[NSPredicate predicateWithFormat:@"SELF.donor_name=%@",];
        
        __block NSDictionary *dicWebResponse;
        HUD=[[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:HUD];
        [HUD showAnimated:YES whileExecutingBlock:^(void)
        {
             dicWebResponse=[SRUtility makeWebServiceCallForPOSTMethod:[NSURL URLWithString:[NSString stringWithFormat:@"http://donations.arearlylearning.org/donation-api-test/createdonor.php"]] withAppendData:StrToPass withRequestHeaderOne:@"User-Agent" withRequestHeaderTwo:@"Content-Type" withHeaderValueOne:@"ASIHTTPRequest" withHeaderValueTwo:@"application/json"];
             }
          completionBlock:^(void)
          {
                 if ([dicWebResponse objectForKey:@"status_message"]) {   //[[NSNotificationCenter defaultCenter]postNotificationName:@"ReloadSignup" object:dicWebResponse];
                     [self recieveDonorDetails:[[dicWebResponse objectForKey:@"data"] objectAtIndex:0]];
                     
                     for (int i=0; i<[[dicWebResponse objectForKey:@"data"] count]; i++) {
                         NSPredicate *ThePredicate=[NSPredicate predicateWithFormat:@"SELF.pin_nbr=%@",[[[dicWebResponse objectForKey:@"data"] objectAtIndex:i] objectForKey:@"pin_nbr"]];
                         NSArray *ArrFltrd=[ArrDonors filteredArrayUsingPredicate:ThePredicate];
                         
                         if ([ArrFltrd count])
                           [ArrDonors removeObject:[ArrFltrd objectAtIndex:0]];
                         [ArrDonors addObject:[[dicWebResponse objectForKey:@"data"] objectAtIndex:0]];
                         NSDictionary *dictData=[[NSDictionary alloc] initWithObjectsAndKeys:ArrDonors, @"data",nil];
                         NSData *dataToSave=[NSKeyedArchiver archivedDataWithRootObject:dictData];
                         [SRUtility adddatatoplist:dataToSave key:@"Donors" plist:@"Data"];
                     }
                     
                     
                    // NSDictionary *dictData=[[NSDictionary alloc] initWithObjectsAndKeys:ArrDonors, @"data",nil];
                    // NSData *dataToSave=[NSKeyedArchiver archivedDataWithRootObject:dictData];
                    // [SRUtility adddatatoplist:dataToSave key:@"Donors" plist:@"Data"];
                     [self dismissViewControllerAnimated:YES completion:Nil];
                     if(!offlineUserSignedUp) {
                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"User successfully signed up." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                         [alert show];
                         offlineUserSignedUp = YES;
                     }
                     [self dismissViewControllerAnimated:YES completion:Nil];
                     [self SignUpData:dicWebResponse];
                 }
             }
         ];
    } else {
//        [SRUtility addtoplist:StrToPass key:@"OfflinDonor" plist:@"Data"];
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AREarlyLearning" message:@"No Network User Will Be Registered To Server When Network is connected" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [alert show];
        
    }
    //    NSDictionary *dictData=[[NSDictionary alloc] initWithObjectsAndKeys:ArrDonors, @"data",nil];
    //    NSData *dataToSave=[NSKeyedArchiver archivedDataWithRootObject:dictData];
    //    [SRUtility adddatatoplist:dataToSave key:@"Donors" plist:@"Data"];
    //    [self dismissViewControllerAnimated:YES completion:Nil];
    //
}

        
-(void)COllectionChangeItem
{
    NSLog(@"COllectionChangeItem");
    NSData *CatData=[SRUtility getdatafromplist:@"Categories" plist:@"Data"];
    NSDictionary *dicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:CatData];
    [ArrCollData removeAllObjects];
    NSLog(@"1");
    NSMutableArray *dataTmp = [dicTmp objectForKey:@"data"];
    for (NSMutableDictionary *dictCurrent in dataTmp) {
        NSLog(@"2");
        [ArrCollData addObject:dictCurrent];
    }
    [_ClctnCategory reloadData];
    [_btnback setHidden:YES];
    [_testButton setHidden:YES];
    [_itemNavigationBtn setHidden:YES];
    collectionFlowTag=1001;
}


-(void)ChangeUserName
{
    NSLog(@"ChangeUserName");
    [_btnUserNme setTitle:[SRUtility getfromplist:@"UserName" plist:@"Data"] forState:UIControlStateNormal];
}
-(IBAction)OverRidePricePopUp:(id)sender
{
    NSLog(@"ChangeUserName");
    NSUserDefaults *ThePrefs=[NSUserDefaults standardUserDefaults];
    [ThePrefs setObject:@"OverRiding" forKey:@"OverRideViewController"];
    [ThePrefs synchronize];
    FormSheetViewController *ObjFormSheet=[self.storyboard instantiateViewControllerWithIdentifier:@"FormSheetViewController"];
    ObjFormSheet.dicItem=[ArrTableData objectAtIndex:[sender tag]];
    ObjFormSheet.categoryChange=self;
    NSString *StrCellTag=[NSString stringWithFormat:@"%d",[sender tag]];
    ObjFormSheet.CellTag=StrCellTag;
    
    [self.parentViewController presentViewController:ObjFormSheet animated:YES completion:nil];
    
    
}

-(void)colleInData
{
    NSLog(@"colleInData");
    NSData *dataCategory=[SRUtility getdatafromplist:@"Categories" plist:@"Data"];
    NSDictionary *DicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:dataCategory];
    ArrCollData=[DicTmp objectForKey:@"data" ];
    NSLog(@"ArrCollData : %@", ArrCollData);
    collectionFlowTag=1001;
    
}

-(void)InitView
{
    NSLog(@"InitView");
    _btnCategory.layer.cornerRadius=16;
    _btnNotofication.layer.cornerRadius=16;
    _btnProPic.layer.cornerRadius=16;
    _btnDonate.layer.cornerRadius=16;
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
   _btnProPic.layer.cornerRadius=_btnProPic.frame.size.height/2;
    _btnProPic.clipsToBounds=YES;
    [_btnProPic setNeedsDisplay];
}
- (void)didReceiveMemoryWarning
{
    NSLog(@"didReceiveMemoryWarning");
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)GrandTotal
{
    NSLog(@"GrandTotal");
    float GrandTotal=0;
    float SubTotal = 0;
    for (NSDictionary *dictCurrent in ArrTableData) {
        StrTotal=[NSString stringWithFormat:@"%@",[dictCurrent objectForKey:@"dollTwo"]];
        SubTotal=[StrTotal floatValue];
        GrandTotal=GrandTotal+SubTotal;
    }
    StrTotal=[NSString stringWithFormat:@"%.2f",GrandTotal];
    [_LblGrandTotal setText:StrTotal];
    [SRUtility addtoplist:StrTotal key:@"TrabsactionTotalAmnt" plist:@"Data"];
}

-(void)BtnDonorClicked
{
    NSLog(@"BtnDonorClicked");
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Coming soon!" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:Nil, nil];
    [alert show];
}
-(void)BtnDailyReportClicked
{
    NSLog(@"BtnDailyReportClicked");
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Coming soon!" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:Nil, nil];
    [alert show];
}
-(void)InitProPic
{
    NSLog(@"InitProPic");
    [_btnProPic setBackgroundImage:[self loadImage] forState:UIControlStateNormal];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (UIImage*)loadImage
{
    NSLog(@"loadImage");
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:@"ProfilPic.png"];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}
- (IBAction)BtnUserNmeClicked:(id)sender
{
    NSLog(@"BtnUserNmeClicked");
    CGPoint Point=_btnUserNme.center;
    ArrPopData=[[NSArray alloc]initWithObjects:@"Change Username",@"Change Password",@"Sign Out", nil];
    // ArrPopData=[[NSArray alloc]initWithObjects:@"Change User Name",@"Change Password",@"Signout", nil];
    TblViewUserPopUp=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 250, 150)];
    TblViewUserPopUp.tag=2;
    TblViewUserPopUp.delegate=self;
    TblViewUserPopUp.dataSource = self;
    TblViewUserPopUp.scrollEnabled=YES;
    TblViewUserPopUp.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
    pv=[PopoverView showPopoverAtPoint:Point inView:self.view withContentView:TblViewUserPopUp delegate:self];
    pv.tag=500;
}
-(void)popoverView:(PopoverView *)popoverView didSelectItemAtIndex:(NSInteger)index
{
    NSLog(@"popoverView");
    [popoverView performSelector:@selector(dismiss) withObject:nil afterDelay:0.3];
    if (pv.tag==500) {
        NSString *StrPop=[ArrPopData objectAtIndex:index];
    
        if ([StrPop isEqualToString:@"Sign Out"]){
            PassWordViewController *objPassWord=[self.storyboard instantiateViewControllerWithIdentifier:@"PassWordViewController"];
            [self.navigationController pushViewController:objPassWord animated:YES];
        }
    }
}

- (IBAction)BtnProPicClicked:(id)sender {
    NSLog(@"BtnProPicClicked");
    ChangeProfilePicViewController *ObjProPic=[self.storyboard instantiateViewControllerWithIdentifier:@"ChangeProfilePicViewController"];
    ObjProPic.ProPicChange=self;
    [self.parentViewController presentViewController:ObjProPic animated:YES completion:nil];
}

#pragma mark - UITableView Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView
{
    NSLog(@"numberOfSectionsInTableView");
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSLog(@"heightForHeaderInSection");
    CGFloat SectionHeight=0;
    SectionHeight=35.0;
    return SectionHeight;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSLog(@"viewForHeaderInSection");
    UIView *ViewTmp;
    if (tableView==TblViewUserPopUp) {
        ViewTmp=[[UIView alloc] initWithFrame:CGRectMake(-2, 0, tableView.frame.size.width+4, 35.0)];
        UILabel *LblTmp=[[UILabel alloc] initWithFrame:CGRectMake(10, 2, tableView.frame.size.width-10, 35.0)];
        NSString *StrProviderName=[NSString stringWithFormat:@"%@",[[[_dicUserDeatils objectForKey:@"data"] objectAtIndex:0] objectForKey:@"user_fname"]];
        NSString *StrFullName=[StrProviderName stringByAppendingString:[NSString stringWithFormat:@"%@",[[[_dicUserDeatils objectForKey:@"data"] objectAtIndex:0] objectForKey:@"user_lname"]]];
        [LblTmp setText:StrFullName];
        [LblTmp setTextAlignment:NSTextAlignmentLeft];
        [ViewTmp addSubview:LblTmp];
    }
    
    if (tableView==TblViewDonation) {
        ViewTmp=[[UIView alloc]initWithFrame:CGRectMake(-2, 0, tableView.frame.size.width+4, 40)];
        [ViewTmp setBackgroundColor:[UIColor whiteColor]];
        UILabel *LblName=[[UILabel alloc]initWithFrame:CGRectMake(15, 2, 70, 40)];
        [LblName setText:@"Name"];
        LblName.font=[UIFont fontWithName:@"Helvetica-Bold" size:18.0];
        [LblName setTextAlignment:NSTextAlignmentLeft];
        [ViewTmp addSubview:LblName];
        UILabel *LblQty=[[UILabel alloc]initWithFrame:CGRectMake(LblName.frame.origin.x+192, 2, 70, 40)];
        [LblQty setText:@"Qty"];
        LblQty.font=[UIFont fontWithName:@"Helvetica-Bold" size:18.0];
        [LblQty setTextAlignment:NSTextAlignmentLeft];
        [ViewTmp addSubview:LblQty];
        UILabel *lblEach=[[UILabel alloc]initWithFrame:CGRectMake(LblQty.frame.origin.x+95, 2, 70, 40)];
        [lblEach setText:@"Each"];
        lblEach.font=[UIFont fontWithName:@"Helvetica-Bold" size:18.0];
        [lblEach setTextAlignment:NSTextAlignmentLeft];
        [ViewTmp addSubview:lblEach];
        UILabel *LblTotal=[[UILabel alloc]initWithFrame:CGRectMake(lblEach.frame.origin.x+78, 2, 70, 40)];
        [LblTotal setText:@"Total"];
        LblTotal.font=[UIFont fontWithName:@"Helvetica-Bold" size:18.0];
        [LblTotal setTextAlignment:NSTextAlignmentLeft];
        [ViewTmp addSubview:LblTotal];
        [self GrandTotal];
    }
    return ViewTmp;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSLog(@"titleForHeaderInSection");
    NSString *StrTitle;
    if(tableView==TblViewUserPopUp)
        StrTitle=[NSString stringWithFormat:@"%@",[ArrPopData objectAtIndex:0]];
    return StrTitle;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"numberOfRowsInSection");
    NSInteger RowCount;
    switch (tableView.tag)
    {     case 1:
            RowCount=[ArrTableData count];
           break;
          case 2:
            RowCount=[ArrPopData count];
            break;
        default:
            break;
    }
    return RowCount;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"heightForRowAtIndexPath");
    CGFloat RowHeight;
    switch (tableView.tag)
    {
        case 1:
            RowHeight=45.0;
            break;
        case 2:
           RowHeight=35.0;
           break;
        default:
            break;
    }
    return RowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"cellForRowAtIndexPath");
    SRTableViewCell *cell=(SRTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"myidentifier"];
    if(cell==nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SRTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
    
    if(tableView==TblViewUserPopUp) {
       cell.LblValue.hidden=YES;
       cell.LblDollOne.hidden=YES;
       cell.LblDollTwo.hidden=YES;
       cell.btnAdd.hidden=YES;
       cell.btnMinus.hidden=YES;
       //cell.LblTblDetails.textColor=[UIColor blackColor];
       cell.LblTblDetails.textColor=[UIColor darkGrayColor];
       cell.LblTblDetails.font = [UIFont boldSystemFontOfSize:12.0];
       cell.LblTblDetails.text=[ArrPopData objectAtIndex:indexPath.row];
       if(indexPath.row==0||indexPath.row==1) {
           cell.backgroundColor=[UIColor grayColor];
       }
       
       //[cell.LblTblDetails setLineBreakMode:UILineBreakModeWordWrap];
       //[cell.LblTblDetails setAdjustsFontSizeToFitWidth:NO];
       //[cell.LblTblDetails setNumberOfLines:0];
       //cell.backgroundColor=[UIColor whiteColor];
       //[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
   }
    
   if (tableView==TblViewDonation) {
       cell.btnAdd.tag=indexPath.row;
       cell.btnMinus.tag=indexPath.row;
       cell.LblTblDetails.text=[[ArrTableData objectAtIndex:indexPath.row] objectForKey:@"TblMaincontent"];
       cell.LblTblDetails.numberOfLines=3;
       cell.LblValue.text=[[ArrTableData objectAtIndex:indexPath.row] objectForKey:@"value"];
       cell.LblDollOne.text=[[ArrTableData objectAtIndex:indexPath.row]objectForKey:@"dollOne"];
       cell.LblDollTwo.text=[[ArrTableData objectAtIndex:indexPath.row] objectForKey:@"dollTwo"];
       cell.LblSplrqst.text=[[ArrTableData objectAtIndex:indexPath.row]objectForKey:@"SplRqst"];
       [cell.btnAdd addTarget:self action:@selector(ValueAdded:) forControlEvents:UIControlEventTouchUpInside];
       tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
       [cell.btnMinus addTarget:self action:@selector(ValueMinus:) forControlEvents:UIControlEventTouchUpInside];
   }
    
   return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"canEditRowAtIndexPath");
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"forRowAtIndexPath");
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [ArrTableData removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [TblViewDonation reloadData];
        [self GrandTotal];
        [ObjDonDetailedView removeFromSuperview];
    }
   
}
- (void)scrollToRowAtIndexPath:(NSIndexPath *)indexPath atScrollPosition:(UITableViewScrollPosition)scrollPosition animated:(BOOL)animated
{
    NSLog(@"scrollToRowAtIndexPath");
    indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [TblViewDonation scrollToRowAtIndexPath:indexPath
                         atScrollPosition:UITableViewScrollPositionBottom
                                 animated:YES];
}
#pragma mark
#pragma mark PopoverDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"didSelectRowAtIndexPath");
    if(tableView==TblViewUserPopUp) {
        [self popoverView:pv didSelectItemAtIndex:indexPath.row];
    }
    
    if (tableView==TblViewDonation) {
    
        NSDictionary *dictCurrent=[ArrTableData objectAtIndex:indexPath.row];
        [ObjDonDetailedView removeFromSuperview];
        [_testButton setHidden:YES];
        [_itemNavigationBtn setHidden:YES];
        ObjDonDetailedView.layer.cornerRadius=8.0;
        [ObjDonDetailedView.ImgDonate setImage:[UIImage imageNamed:[dictCurrent objectForKey:@"image"]]];
        [ObjDonDetailedView.LblDonateDetails setText:[dictCurrent objectForKey:@"TblMaincontent"]];
        [ObjDonDetailedView.LblDescription setText:[dictCurrent objectForKey:@"itemDescription"]];
        ObjDonDetailedView.btnAdd.tag=indexPath.row;
        ObjDonDetailedView.btnMinus.tag=indexPath.row;
        ObjDonDetailedView.btnOverRidePriice.tag=indexPath.row;
        ObjDonDetailedView.btnModifier.tag=indexPath.row;
        ObjDonDetailedView.SplRequestSave.tag=indexPath.row;
        [ObjDonDetailedView.LblValue setText:[dictCurrent objectForKey:@"value"]];
        [ObjDonDetailedView.specialRequestTxt setText:[dictCurrent objectForKey:@"SplRqst"]];
        [self.ViewDonateDetails addSubview:ObjDonDetailedView];
        ViewCreated=YES;
    }
    
}
-(IBAction)DonationIncrement:(id)sender
{
    NSLog(@"DonationIncrement");
    if (sender==ObjDonDetailedView.btnAdd)
      [self ValueAdded:sender];
    if (sender==ObjDonDetailedView.btnMinus)
        [self ValueMinus:sender];
    if (sender==ObjDonDetailedView.btnModifier)
        [sender ValueAdded:sender];
        
    
    [ObjDonDetailedView.LblValue setText:[[ArrTableData objectAtIndex:[sender tag]] objectForKey:@"value"]];
    
}
-(IBAction)specialRequestSaved:(id)sender
{
    NSLog(@"specialRequestSaved");
    int tag=[sender tag];
    
    NSMutableDictionary *dictCurrent=[ArrTableData objectAtIndex:tag];
    [dictCurrent setObject:ObjDonDetailedView.specialRequestTxt.text forKey:@"SplRqst"];
    [ArrTableData replaceObjectAtIndex:tag withObject:dictCurrent];
    [TblViewDonation reloadData];
    [SRUtility addtoplist:ObjDonDetailedView.specialRequestTxt.text key:@"SpecialRequest" plist:@"Data"];
    NSString*test=[SRUtility getfromplist:@"SpecialRequest" plist:@"Data"];
    NSLog(@"%@",test);
    NSData *CatData=[SRUtility getdatafromplist:@"Categories" plist:@"Data"];
    NSDictionary *dicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:CatData];
    [ObjDonDetailedView.specialRequestTxt resignFirstResponder];
    [ObjDonDetailedView removeFromSuperview];
    [ObjDonDetailedView.specialRequestTxt setText:@""];
    [ArrCollData removeAllObjects];
    for (NSMutableDictionary *dictCurrent in [dicTmp objectForKey:@"data"])
    {
        [ArrCollData addObject:dictCurrent];
        
    }
    [_ClctnCategory reloadData];
    [_btnback setHidden:YES];
    [_testButton setHidden:YES];
    [_itemNavigationBtn setHidden:YES];
    collectionFlowTag=1001;

}

- (IBAction)BtnCategoryClicked:(id)sender
{
    NSLog(@"BtnCategoryClicked");
   [ObjDonDetailedView removeFromSuperview];
   NSData *CatData=[SRUtility getdatafromplist:@"Categories" plist:@"Data"];
    NSDictionary *dicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:CatData];
    [ArrCollData removeAllObjects];
    for (NSMutableDictionary *dictCurrent in [dicTmp objectForKey:@"data"]) {
        [ArrCollData addObject:dictCurrent];
    }
    [_ClctnCategory reloadData];
    [_btnback setHidden:YES];
    [_testButton setHidden:YES];
    [_itemNavigationBtn setHidden:YES];
    collectionFlowTag=1001;
}

- (IBAction)BtnNotificationClicked:(id)sender
{
    NSLog(@"BtnNotificationClicked");
    [ObjDonDetailedView removeFromSuperview];
    NSData *CatData=[SRUtility getdatafromplist:@"Categories" plist:@"Data"];
    NSDictionary *dicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:CatData];
    [ArrCollData removeAllObjects];
    for (NSMutableDictionary *dictCurrent in [dicTmp objectForKey:@"data"])
    {
        [ArrCollData addObject:dictCurrent];
        
    }
    [_ClctnCategory reloadData];
    [_btnback setHidden:YES];
    [_testButton setHidden:YES];
    [_itemNavigationBtn setHidden:YES];
    collectionFlowTag=1001;
}

#pragma mark
#pragma mark valuechanged
-(IBAction)ValueAdded:(id)sender
{
    NSLog(@"ValueAdded");
    ViewCreated=YES;
    int tag=[sender tag];
    NSString *StrCurrValue=[[ArrTableData objectAtIndex:tag] objectForKey:@"value"];
    NSString *StrDollEach=[[ArrTableData objectAtIndex:tag]objectForKey:@"dollOne"];
    float DollEach=[StrDollEach floatValue];
    Valuesub=[StrCurrValue floatValue];
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    NSString *StrPrefs=[prefs objectForKey:@"OverRideViewController"];
    if (!([StrPrefs isEqualToString:@"ModifyQuantity"]||[StrPrefs isEqualToString:@"DailyReport"])) {
        Valuesub++;
    }
    if (Valuesub==0) Valuesub=1;
    float DollEachTotal=DollEach*Valuesub;
    NSString *StrDollTotal=[NSString stringWithFormat:@"%.2f",DollEachTotal];
    NSString *StrValue=[NSString stringWithFormat:@"%.2f",Valuesub];
    [ArrTableData removeObject:[[ArrTableData objectAtIndex: tag] objectForKey:@"value"]];
    [[ArrTableData objectAtIndex:tag] setObject:StrValue forKey:@"value"];
    if (ViewCreated==YES)
      [ObjDonDetailedView.LblValue setText:StrValue];
    //[self DonationIncrement:sender];
    //[[ArrTableData objectAtIndex:tag] setObject:StrDollTotal forKey:@"dollOne"];
    [[ArrTableData objectAtIndex:tag]setObject:StrDollTotal forKey:@"dollTwo"];
    [TblViewDonation reloadData];
    ViewCreated=NO;
    
}

-(IBAction)ValueMinus:(id)sender
{
    NSLog(@"ValueMinus");
    ViewCreated=YES;
    NSString *StrCurrValue=[[ArrTableData objectAtIndex:[sender tag]]objectForKey:@"value"];
    Valuesub=[StrCurrValue intValue];
    NSString *StrDollEach=[[ArrTableData objectAtIndex:[sender tag]]objectForKey:@"dollOne"];
    float DollEach=[StrDollEach floatValue];
    Valuesub=[StrCurrValue floatValue];
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    NSString *StrPrefs=[prefs objectForKey:@"OverRideViewController"];

    if (!([StrPrefs isEqualToString:@"ModifyQuantity"]||[StrPrefs isEqualToString:@"DailyReport"])) {
        Valuesub--;
    }
    
    if (Valuesub==0) Valuesub=1;
    
    float DollEachTotal=DollEach*Valuesub;
    NSString *StrDollTotal=[NSString stringWithFormat:@"%.2f",DollEachTotal];
    NSString *Strvalue=[NSString stringWithFormat:@"%.2f",Valuesub];
    [ArrTableData removeObject:[[ArrTableData objectAtIndex:[sender tag]] objectForKey:@"value"]];
    [[ArrTableData objectAtIndex:[sender tag]] setObject:Strvalue forKey:@"value"];
    if (ViewCreated==YES)
        [ObjDonDetailedView.LblValue setText:Strvalue];

    //[self DonationIncrement:sender];
    //[[ArrTableData objectAtIndex:[sender tag]] setObject:StrDollTotal forKey:@"dollOne"];
    [[ArrTableData objectAtIndex:[sender tag]]setObject:StrDollTotal forKey:@"dollTwo"];
    [TblViewDonation reloadData];
    ViewCreated=NO;
}

-(void)SignUpData:(NSDictionary *)dictData
{
    NSLog(@"SignUpData");
    NSUserDefaults *prefes=[NSUserDefaults standardUserDefaults];
    [prefes setObject:@"DonorSelected" forKey:@"DonorSelection"];
    [prefes synchronize];
    _donorDetails=[[dictData objectForKey:@"data"] objectAtIndex:0];
    [_LblDonorName setHidden:NO];
    [_LblDonorName setText:[[[dictData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"donor_name"]];
    
    
}


-(void)TableReload
{
    NSLog(@"TableReload");
    ArrTableData=[[NSMutableArray alloc]init];
    [TblViewDonation reloadData];
    [_LblGrandTotal setText:@"0.00"];
    [_LblDonorName setText:@""];
    [_LblDonorName setHidden:YES];
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    [prefs setObject:@"DonorNotSelected" forKey:@"DonorSelection"];
    [prefs synchronize];
    __block NSDictionary *transaction;
    HUD=[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:HUD];
    [HUD showAnimated:YES whileExecutingBlock:^(void) {
         transaction=[SRUtility makeWebServicecallForGetmethod:[NSURL URLWithString:[NSString stringWithFormat:@"http://donations.arearlylearning.org/donation-api-test/index.php?request=transaction"]]];
     }
      completionBlock:^(void)
     { NSDecimalNumber *transactionID=transaction[@"data"];
      stringOFTransaction=[NSString stringWithFormat:@"%@",transactionID];
         if([stringOFTransaction isEqualToString:@"(null)"]){
             _labltrasactionId.text=@"";
         }
         else
         _labltrasactionId.text=stringOFTransaction;
     }];

    if (ObjDonDetailedView)
    {
        [ObjDonDetailedView removeFromSuperview];
    }
    
}

-(void)ReloadTablFromOtherView:(NSDictionary *)dictCategory CellTag:(NSString*)Tag
{
    NSLog(@"ReloadTablFromOtherView");
    if ([dictCategory objectForKey:@"OverRidePrice"])
    {  int tag=[Tag intValue];
       
        [ArrTableData replaceObjectAtIndex:tag withObject:dictCategory];
        [TblViewDonation reloadData];
       
    }
    else if ([dictCategory objectForKey:@"Modifier"])
    {    int tag=[Tag intValue];
        
        [ArrTableData replaceObjectAtIndex:tag withObject:dictCategory];
        [self performSelector:@selector(ValueAdded:) withObject:ObjDonDetailedView.btnModifier afterDelay:0.3];
        [TblViewDonation reloadData];
        
    }
    else
    {
        [ArrTableData addObject:dictCategory];
        [TblViewDonation setContentOffset:CGPointMake(0,[TblViewDonation rowHeight]*[ArrTableData count])];
        [TblViewDonation reloadData];
    }
   
}
- (IBAction)BtnDonateClicked:(id)sender
{
    NSLog(@"BtnDonateClicked");
    NSUserDefaults *Prefs=[NSUserDefaults standardUserDefaults];
    [Prefs setObject:@"FromDonateButton" forKey:@"ArrayLastObjectRemoval"];
    
    NSData *DataOffline=[SRUtility getdatafromplist:@"OfflineTransaction" plist:@"Data"];
    ArrForSavingData=[NSKeyedUnarchiver unarchiveObjectWithData:DataOffline];
    
    [_btnDonor setUserInteractionEnabled:YES];
    
    if([ArrForSavingData count]==0) {
        if (![DataOffline length]||[ArrForSavingData count]==0) {
            ArrForSavingData=[[NSMutableArray alloc]init];
        }
    }
    
    NSMutableArray *ArrToServer=[[NSMutableArray alloc]init];
    for (NSDictionary *dictCurrent in ArrTableData ) {
        NSMutableDictionary *dictData=[[NSMutableDictionary alloc]init];
        [dictData setObject:[dictCurrent objectForKey:@"item_id"] forKey:@"item_id"];
        [dictData setObject:[dictCurrent objectForKey:@"TblMaincontent"] forKey:@"item_name"];
        [dictData setObject:[dictCurrent objectForKey:@"value"] forKey:@"qty"];
        [dictData setObject:[dictCurrent objectForKey:@"dollOne"] forKey:@"unit_price"];
        if ([dictCurrent objectForKey:@"SplRqst"]) {
            [dictData setObject:[dictCurrent objectForKey:@"SplRqst"] forKey:@"special_request"];
        } else {
            [dictData setObject:@"" forKey:@"special_request"];
        }
        [ArrToServer addObject:dictData];
        
    }
    
    NSDate *Now=[NSDate date];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSString *StrDate=[dateFormatter stringFromDate:Now];
    
    NSData *data=[NSKeyedArchiver archivedDataWithRootObject:ArrTableData];
    [SRUtility adddatatoplist:data key:@"ArrDonateData" plist:@"Data"];
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    NSString *StrDonoSelection=[prefs objectForKey:@"DonorSelection"];
    
    
    if ([StrDonoSelection isEqualToString:@"DonorSelected"]) {
        if (_donorDetails) {
             if([ArrToServer count]) {
                 NSString *totalAmntOftransaction=[SRUtility getfromplist:@"TrabsactionTotalAmnt" plist:@"Data"];
                 NSUserDefaults *Prefs=[NSUserDefaults standardUserDefaults];
                 NSString *Userpin=[Prefs objectForKey:@"UserPinForLogin"];
                 NSMutableArray *ArrDonations=[[NSMutableArray alloc]init];
                 NSMutableDictionary *dicDonors=[[NSMutableDictionary alloc]init];
                 [dicDonors setObject:ArrToServer forKey:@"donations"];
                 [dicDonors setObject:StrDate forKey:@"date_time"];
                 [dicDonors setObject:totalAmntOftransaction forKey:@"grand_total"];
                 [dicDonors setObject:Userpin forKey:@"user_pin"];
                 [dicDonors setObject:[_donorDetails objectForKey:@"email"] forKey:@"donor_email"];
                 [dicDonors setObject:[_donorDetails objectForKey:@"donor_name"] forKey:@"donor_name"];
                 //if ([stringOFTransaction length])
                   [dicDonors setObject:@"" forKey:@"transactionID"];
                 // [dicDonors setObject:[SRUtility uuid] forKey:@"temp_id"];
                 
                 [ArrDonations addObject:dicDonors];
                 
                 
                 NSDictionary *dictToPass=[[NSDictionary alloc]initWithObjectsAndKeys:ArrDonations,@"donors",[_donorDetails objectForKey:@"pin_nbr"],@"pin_nbr",@"0",@"server_recieved",@"Yes",@"AlreadyDonated", nil];
                 NSMutableArray *ArrToPost=[[NSMutableArray alloc]initWithObjects:dictToPass, nil];
                 
                 //NSString *StrPinNmbr=[NSString stringWithFormat:@"%@",[dictToPass objectForKey:@"pin_nbr"]];
                 
                 [ArrForSavingData addObject:dictToPass];
                 
                 NSData *data=[NSKeyedArchiver archivedDataWithRootObject:ArrForSavingData];
                 [SRUtility adddatatoplist:data key:@"OfflineTransaction" plist:@"Data"];
                SignatureViewController *ObjSignature = [self.storyboard instantiateViewControllerWithIdentifier:@"SignatureViewController"];
                ObjSignature.ArrToServer=ArrToPost;
                ObjSignature.ArrData=ArrTableData;
                ObjSignature.dicDonor=_donorDetails;
                ObjSignature.StrCurrentdate=StrDate;
                ObjSignature.specialNote=splRqstNote;
                 
                //ObjSignature.DelegateTableReload=self;
                [self.parentViewController presentViewController: ObjSignature animated:YES completion:Nil];
            } else {
                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Please select any items." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 [alert show];
             }
          } else {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Please select a donor." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
               [alert show];
          }
    } else {
       UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Please select a donor." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
       [alert show];
    }

    
}
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    NSLog(@"numberOfSectionsInCollectionView");
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSLog(@"numberOfItemsInSection");
    return [ArrCollData count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  NSLog(@"cellForItemAtIndexPath");
  SRCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"SRCollectionViewCell" forIndexPath:indexPath];
  [cell.LblCategory setText:[[ArrCollData objectAtIndex:indexPath.row] objectForKey:@"category_name"]];
  [cell.ImgCategory setImage:[UIImage imageNamed:[[ArrCollData objectAtIndex:indexPath.row] objectForKey:@"category_image"]]];
    
  return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(!currentlyAddingDonationItem) {
        NSLog(@"didSelectItemAtIndexPath");
        NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
        NSString *StrDonoSelection=[prefs objectForKey:@"DonorSelection"];
      
        if ([StrDonoSelection isEqualToString:@"DonorSelected"]) {
            NSDictionary *dictCurrent;
            
            if ([ArrCollData count]) {
              dictCurrent=[ArrCollData objectAtIndex:indexPath.row];
            } else {
                [self colleInData];
                dictCurrent=[ArrCollData objectAtIndex:indexPath.row];
                collectionFlowTag=1001;
            }
       
            if (collectionFlowTag==1001) {
                [_btnback setHidden:YES];
                [_testButton setHidden:NO];
                [_itemNavigationBtn setHidden:YES];
                NSString *testbtnlabel=[dictCurrent objectForKey:@"category_name"];
                if([testbtnlabel isEqualToString:@"Automobile (Miles)"]){
                    testbtnlabel=@"Automobile";
                }
                
                NSString *testbtnspacedLabel=[NSString stringWithFormat:@"      %@",testbtnlabel];
                
                [_testButton setTitle:testbtnspacedLabel forState:UIControlStateNormal];
                
                [ArrCollData removeAllObjects];
                
                if ([[dictCurrent objectForKey:@"category_name"] isEqualToString:@"Automobile (Miles)"]) {
                    NSPredicate *thePredicate=[NSPredicate predicateWithFormat:@"SELF.categories_id=%@",[dictCurrent objectForKey:@"category_id"]];
                    NSData *DataSubCategory=[SRUtility getdatafromplist:@"Items" plist:@"Data"];
                    NSDictionary *dictTmp=[NSKeyedUnarchiver unarchiveObjectWithData:DataSubCategory];
                    NSArray *ArrFltrd=[[dictTmp objectForKey:@"data"] filteredArrayUsingPredicate:thePredicate];
                    for (NSDictionary *dictCurrent in ArrFltrd ) {
                        NSMutableDictionary *dicTabledata=[[NSMutableDictionary alloc]init];
                        [dicTabledata setObject:[dictCurrent objectForKey:@"item_image"] forKey:@"image"];
                        [dicTabledata setObject:[dictCurrent objectForKey:@"item_name"] forKey:@"TblMaincontent"];
                        [dicTabledata setObject:[dictCurrent objectForKey:@"item_description"]forKey:@"itemDescription"];
                        [dicTabledata setObject:[dictCurrent objectForKey:@"modifier"] forKey:@"value"];
                        [dicTabledata setObject:[dictCurrent objectForKey:@"unit_price"] forKey:@"dollOne"];
                        [dicTabledata setObject:[dictCurrent objectForKey:@"unit_price"] forKey:@"dollTwo"];
                        [dicTabledata setObject:[dictCurrent objectForKey:@"item_id"] forKey:@"item_id"];
                        [ArrTableData addObject:dicTabledata];
                    }
                    
                    collectionFlowTag=2001;
                    currentlyAddingDonationItem = YES;
                    
                    [TblViewDonation setContentOffset:CGPointMake(0,[TblViewDonation rowHeight]*[ArrTableData count])];
                    [TblViewDonation reloadData];
                    
                } else {
                    NSPredicate *ThePredciate=[NSPredicate predicateWithFormat:@"SELF.category_id=%@",[dictCurrent objectForKey:@"category_id"]];
                    
                    NSData *dataSubCategory=[SRUtility getdatafromplist:@"SubCategories"plist:@"Data"];
                    NSDictionary *dicTmp= [NSKeyedUnarchiver unarchiveObjectWithData:dataSubCategory];
                    NSArray *ArrFltrd=[[dicTmp objectForKey:@"data"] filteredArrayUsingPredicate:ThePredciate];
                    
                    for ( NSMutableDictionary *dictCrrnt in ArrFltrd) {
                        [dictCrrnt setObject:[dictCrrnt objectForKey:@"sub_category_name"] forKey:@"category_name"];
                        [dictCrrnt setObject:[dictCrrnt objectForKey:@"sub_category_image"] forKey:@"category_image"];
                        [ArrCollData addObject:dictCrrnt];
                    }
                    
                    [_ClctnCategory reloadData];
                }
                collectionFlowTag=2001;
            } else if (collectionFlowTag==2001) {
                [_itemNavigationBtn setHidden:NO];
                [_btnback setHidden:YES];
                [_testButton setHidden:NO];

                NSString *itembtnlabel=[dictCurrent objectForKey:@"category_name"];
                if([itembtnlabel isEqualToString:@"Home School Connections"]){
                    itembtnlabel=@"Home School";
                }
                NSString *itembtnLabelSpaced=[NSString stringWithFormat:@"    %@",itembtnlabel];
                [_itemNavigationBtn setTitle:itembtnLabelSpaced forState:UIControlStateNormal];
                [ArrCollData removeAllObjects];


                NSPredicate *ThePreciate=[NSPredicate predicateWithFormat:@"SELF.sub_category_id=%@",[dictCurrent objectForKey:@"sub_category_id"]];

                NSData *dataItems=[SRUtility getdatafromplist:@"Items"plist:@"Data"];

                NSDictionary *dicTmp= [NSKeyedUnarchiver unarchiveObjectWithData:dataItems];
                NSArray *ArrFltrd=[[dicTmp objectForKey:@"data"] filteredArrayUsingPredicate:ThePreciate];
                for (NSMutableDictionary *dictCurrent in ArrFltrd ) {
                  [dictCurrent setObject:[dictCurrent objectForKey:@"item_name"] forKey:@"category_name"];
                  [dictCurrent setObject:[dictCurrent objectForKey:@"item_image"] forKey:@"category_image"];
                  [ArrCollData addObject:dictCurrent];
                  
                  
                }
                [_ClctnCategory reloadData];


                collectionFlowTag=5001;
                
            } else if (collectionFlowTag==5001) {
                
                [_btnback setHidden:YES];
                if ([[dictCurrent objectForKey:@"unit_price"] isEqualToString:@"0.00"])
                {    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
                    [prefs setObject:@"Nothing" forKey:@"OverRideViewController"];
                    [prefs synchronize];
                    NSDictionary *dictToCurrentTbl=[ArrCollData objectAtIndex:indexPath.row];
                    FormSheetViewController *objFormSheet=[self.storyboard instantiateViewControllerWithIdentifier:@"FormSheetViewController"];
                    objFormSheet.strProDescp=[dictToCurrentTbl objectForKey:@"item_description"];
                    //item_description
                    objFormSheet.StrProName=[dictToCurrentTbl objectForKey:@"item_name"];
                    objFormSheet.dicItem=dictToCurrentTbl;
                    objFormSheet.categoryChange=self;
                    [self.parentViewController presentViewController:objFormSheet animated:YES completion:Nil];
                } else {
                    NSDictionary *dictToCurrentTbl=[ArrCollData objectAtIndex:indexPath.row];
                    NSMutableDictionary *dicTabledata=[[NSMutableDictionary alloc]init];
                    [dicTabledata setObject:[dictToCurrentTbl objectForKey:@"category_image"] forKey:@"image"];
                    [dicTabledata setObject:[dictToCurrentTbl objectForKey:@"item_name"] forKey:@"TblMaincontent"];
                    [dicTabledata setObject:[dictToCurrentTbl objectForKey:@"item_description"]forKey:@"itemDescription"];
                    [dicTabledata setObject:@"1" forKey:@"value"];
                    [dicTabledata setObject:[dictToCurrentTbl objectForKey:@"unit_price"] forKey:@"dollOne"];
                    [dicTabledata setObject:[dictToCurrentTbl objectForKey:@"unit_price"] forKey:@"dollTwo"];
                    [dicTabledata setObject:[dictToCurrentTbl objectForKey:@"item_id"] forKey:@"item_id"];
                    [ArrTableData addObject:dicTabledata];
                    currentlyAddingDonationItem = YES;
                    
                    [TblViewDonation setContentOffset:CGPointMake(0,[TblViewDonation rowHeight]*[ArrTableData count])];
                    [TblViewDonation reloadData];
                    collectionFlowTag=5001;
                }
            }
            // prevent rapid adding of items
            dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 1.00);
            dispatch_after(delay, dispatch_get_main_queue(), ^(void){
                currentlyAddingDonationItem = NO;
            });
        } else
        {

            [self openDonors];
        }
    }
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return !currentlyAddingDonationItem;
}

- (IBAction)BtnDailyReportClicked:(id)sender
{
    NSLog(@"BtnDailyReportClicked");
    
    DailyReportsViewController *objFormSheet=[self.storyboard instantiateViewControllerWithIdentifier:@"DailyReportsViewController"];
    objFormSheet.ReportDelegate=self;
    [self.parentViewController presentViewController:objFormSheet animated:YES completion:Nil];

    
}
#pragma mark OCCalendarDelegate Methods
-(void)DateSelectedOCCalendar
{
    NSLog(@"DateSelectedOCCalendar");
    [calVC removeCalView];
}

- (void)completedWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate
{
    NSLog(@"completedWithStartDate");
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateStyle:NSDateFormatterShortStyle];
    NSString *StrTmpDate=@"";
    StrTmpDate=[StrTmpDate StringFrom_cDate:startDate];
    [calVC.view removeFromSuperview];
    calVC.delegate = nil;
    calVC = nil;
}

-(void) completedWithNoSelection {
    NSLog(@"completedWithNoSelection");
    [calVC.view removeFromSuperview];
    calVC.delegate = nil;
    calVC = nil;
}

-(void)openDonors {
    if([SRUtility reachable]) {
        [_labltrasactionId setHidden:NO];
        //[_btnSave setHidden:NO];
        DonorDetailsViewController *ObjDonors=[self.storyboard instantiateViewControllerWithIdentifier:@"DonorDetailsViewController"];
        ObjDonors.DonorChange=self;
        [self.parentViewController presentViewController:ObjDonors animated:YES completion:Nil];
    } else {
        NSData *data=[SRUtility getdatafromplist:@"Donations" plist:@"Data"];
        __block NSMutableArray *ArrToSave=[NSKeyedUnarchiver unarchiveObjectWithData:data];
        if([ArrToSave count] < 10) {
            [_labltrasactionId setHidden:NO];
            //[_btnSave setHidden:NO];
            DonorDetailsViewController *ObjDonors=[self.storyboard instantiateViewControllerWithIdentifier:@"DonorDetailsViewController"];
            ObjDonors.DonorChange=self;
            [self.parentViewController presentViewController:ObjDonors animated:YES completion:Nil];
        } else {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AREarlyLearning" message:@"You may not have more that 10 transactions at a time without an internet connection.  Please connect to the internet and sync your current transactions." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (IBAction)BtnDonorClicked:(id)sender
{
    NSLog(@"BtnDonorClicked");
    [self openDonors];
}
-(void)presentEmailView
{
    NSLog(@"presentEmailView");
    EmailViewController *ObjEmail=[self.storyboard instantiateViewControllerWithIdentifier:@"EmailViewController"];
    [self.parentViewController presentViewController:ObjEmail animated:YES completion:nil];
}
-(void)presentSignatureView
{
    NSLog(@"presentSignatureView");
    SignatureViewController *ObjSign=[self.storyboard instantiateViewControllerWithIdentifier:@"SignatureViewController"];
    [self.parentViewController presentViewController:ObjSign animated:YES completion:nil];
}
- (IBAction)BackBtnClicked:(id)sender
{
    NSLog(@"BackBtnClicked");
    NSDictionary *dictTmp=[ArrCollData objectAtIndex:0];
    if([dictTmp objectForKey:@"item_id"]) {
        NSPredicate *ThePredicate=[NSPredicate predicateWithFormat:@"SELF.category_id=%@",[dictTmp objectForKey:@"categories_id"]];
        NSData *SubCatData=[SRUtility getdatafromplist:@"SubCategories" plist:@"Data"];
        NSDictionary *dicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:SubCatData];
        NSArray *ArrFltrd=[[dicTmp objectForKey:@"data"] filteredArrayUsingPredicate:ThePredicate];
        [ArrCollData removeAllObjects];
        for (NSMutableDictionary *dictCurrent in ArrFltrd )
        {
            [dictCurrent setObject:[dictCurrent objectForKey:@"sub_category_name"] forKey:@"category_name"];
            [dictCurrent setObject:[dictCurrent objectForKey:@"sub_category_image"] forKey:@"category_image"];
            [ArrCollData addObject:dictCurrent];
            
        }
        [_ClctnCategory reloadData];
        collectionFlowTag=2001;
        [_itemNavigationBtn setHidden:YES];
    } else {
       
        NSData *CatData=[SRUtility getdatafromplist:@"Categories" plist:@"Data"];
        NSDictionary *dicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:CatData];
               [ArrCollData removeAllObjects];
        for (NSMutableDictionary *dictCurrent in [dicTmp objectForKey:@"data"])
        {
            [ArrCollData addObject:dictCurrent];
            
        }
        [_ClctnCategory reloadData];
        [_btnback setHidden:YES];
        [_testButton setHidden:YES];
        [_itemNavigationBtn setHidden:YES];
        collectionFlowTag=1001;
    }
}
- (IBAction)subCategoryNavigation:(id)sender {
    NSLog(@"subCategoryNavigation");
    NSData *CatData=[SRUtility getdatafromplist:@"Categories" plist:@"Data"];
    NSDictionary *dicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:CatData];
    [ArrCollData removeAllObjects];
    for (NSMutableDictionary *dictCurrent in [dicTmp objectForKey:@"data"])
    {
        [ArrCollData addObject:dictCurrent];
        
    }
    [_ClctnCategory reloadData];
    [_btnback setHidden:YES];
    [_testButton setHidden:YES];
    [_itemNavigationBtn setHidden:YES];
    collectionFlowTag=1001;

    
}
- (IBAction)itemNavigationBtn:(id)sender {
    NSLog(@"itemNavigationBtn");
    NSDictionary *dictTmp=[ArrCollData objectAtIndex:0];
    
        NSPredicate *ThePredicate=[NSPredicate predicateWithFormat:@"SELF.category_id=%@",[dictTmp objectForKey:@"categories_id"]];
        NSData *SubCatData=[SRUtility getdatafromplist:@"SubCategories" plist:@"Data"];
        NSDictionary *dicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:SubCatData];
        NSArray *ArrFltrd=[[dicTmp objectForKey:@"data"] filteredArrayUsingPredicate:ThePredicate];
        [ArrCollData removeAllObjects];
        for (NSMutableDictionary *dictCurrent in ArrFltrd )
        {
            [dictCurrent setObject:[dictCurrent objectForKey:@"sub_category_name"] forKey:@"category_name"];
            [dictCurrent setObject:[dictCurrent objectForKey:@"sub_category_image"] forKey:@"category_image"];
            [ArrCollData addObject:dictCurrent];
            
        }
        [_ClctnCategory reloadData];
        collectionFlowTag=2001;
        [_itemNavigationBtn setHidden:YES];
    
}


-(void)recieveDonorDetails:(NSDictionary *)dictDonor
{
    NSLog(@"recieveDonorDetails");
    [_LblDonorName setHidden:NO];
    //_LblDonorName.layer.borderWidth=1.0;
    CALayer *lbl=[_LblDonorName layer];
    [lbl setMasksToBounds:YES];
    [lbl setCornerRadius:8.0];
   // _LblDonorName.layer.cornerRadius=8.0;
    _donorDetails=dictDonor;
    [_LblDonorName setText:[dictDonor objectForKey:@"donor_name"]];


}


#pragma mark
#pragma mark Search Functionality
-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSLog(@"filterContentForSearchText");
    [searchResults removeAllObjects];
    
    if (!searchText.length)
    {
        NSData *DataDonors=[SRUtility getdatafromplist:@"Donors" plist:@"Data"];
        NSDictionary *DicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:DataDonors];
        ArrTableData=[DicTmp objectForKey:@"data"];
        
    } else
    {
        // Filter the array using NSPredicate
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.category_name contains[c] %@",searchText];
        searchResults = [NSMutableArray arrayWithArray:[ArrCollData filteredArrayUsingPredicate:predicate]];
        ArrCollData=[searchResults mutableCopy];
    }
    
    
    [_ClctnCategory reloadData];
    
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"searchBarSearchButtonClicked");
    [self.searchBar resignFirstResponder];
}

#pragma mark - UISearchDisplayController Delegate Methods
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    NSLog(@"searchDisplayController");
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    //[self positionForBar:0];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    NSLog(@"shouldReloadTableForSearchScope");
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"searchBarCancelButtonClicked");
    //ArrData=[appDelegate getDonors];
    [self.searchBar resignFirstResponder];
    if (collectionFlowTag==1001) {
        NSData *dataCategory=[SRUtility getdatafromplist:@"Categories" plist:@"Data"];
        NSDictionary *DicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:dataCategory];
        ArrCollData=[DicTmp objectForKey:@"data" ];
        
    }
    if (collectionFlowTag==2001) {
        NSData *dataSubCategory=[SRUtility getdatafromplist:@"SubCategories"plist:@"Data"];
        NSDictionary *dicTmp= [NSKeyedUnarchiver unarchiveObjectWithData:dataSubCategory];
        ArrCollData=[dicTmp objectForKey:@"data"];
    }
//    if (collectionFlowTag==5001) {
//        
//    }
    NSData *DataDonors=[SRUtility getdatafromplist:@"Donors" plist:@"Data"];
    NSDictionary *DicTmp=[NSKeyedUnarchiver unarchiveObjectWithData:DataDonors];
    ArrTableData=[DicTmp objectForKey:@"data"];
    
    [_ClctnCategory reloadData];
    // [_TblSettings scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    NSLog(@"textFieldShouldClear");
    //if we only try and resignFirstResponder on textField or searchBar,
    //the keyboard will not dissapear (at least not on iPad)!
    [self performSelector:@selector(searchBarCancelButtonClicked:) withObject:self.searchBar afterDelay: 0.1];
    return YES;
}

-(void)PresentDonorSignUp
{
    NSLog(@"PresentDonorSignUp");
    DonorSignUpViewController *ObjDonSignUp=[self.storyboard instantiateViewControllerWithIdentifier:@"DonorSignUpViewController"];
    ObjDonSignUp.DonorDelegate=self;
    [self.parentViewController presentViewController:ObjDonSignUp animated:YES completion:Nil];
  
}

-(void)PresentDonors
{
    NSLog(@"PresentDonors");
    DonorDetailsViewController *ObjDon=[self.storyboard instantiateViewControllerWithIdentifier:@"DonorDetailsViewController"];
    [self.parentViewController presentViewController:ObjDon animated:YES completion:Nil];
    
}

//-(void)IncrementeTransaction
//{
//   
//}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    NSLog(@"textViewDidBeginEditing");
    [self animateTextView: YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    NSLog(@"textViewDidEndEditing");
    [self animateTextView:NO];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSLog(@"shouldChangeTextInRange");
    
    if(text)
    {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (void) animateTextView:(BOOL) up
{
    NSLog(@"animateTextView");
    const int movementDistance =390; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    int movement= movement = (up ? -movementDistance : movementDistance);
    NSLog(@"%d",movement);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.inputView.frame, 0, movement);
    [UIView commitAnimations];
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    NSLog(@"textFieldShouldBeginEditing");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    textField.placeholder=Nil;
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    NSLog(@"textFieldShouldEndEditing");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"textFieldDidEndEditing");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];

    
    NSLog(@"%@",textField.text);
   // splRqstNote=textField.text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"textFieldShouldReturn");
       [textField resignFirstResponder];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
        
        [self.view endEditing:YES];
    
    
    return YES;
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    NSLog(@"keyboardDidShow");
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-365,1024,714)]; //here taken -20 for example i.e. your view will be scrolled to -20. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    NSLog(@"keyboardDidHide");
    [self.view setFrame:CGRectMake(0,0,1024,714)];
}

- (IBAction)BtnSaveClicked:(id)sender
{
    NSLog(@"BtnSaveClicked");
    NSUserDefaults *Prefs=[NSUserDefaults standardUserDefaults];
    [Prefs setObject:@"FromSaveButton" forKey:@"ArrayLastObjectRemoval"];
    NSData *data=[SRUtility getdatafromplist:@"Donations" plist:@"Data"];
    NSMutableArray *ArrToSave=[NSKeyedUnarchiver unarchiveObjectWithData:data];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"CollectionReload" object:nil];
    NSData *DataOffline=[SRUtility getdatafromplist:@"OfflineTransaction" plist:@"Data"];
    ArrForSavingData=[NSKeyedUnarchiver unarchiveObjectWithData:DataOffline];
    if (![DataOffline length]||[ArrForSavingData count]==0) {
        ArrForSavingData=[[NSMutableArray alloc]init];
    }
    
    NSMutableArray *ArrToServer=[[NSMutableArray alloc]init];
    
    for (NSDictionary *dictCurrent in ArrTableData ) {
        NSMutableDictionary *dictData=[[NSMutableDictionary alloc]init];
        [dictData setObject:[dictCurrent objectForKey:@"item_id"] forKey:@"item_id"];
        [dictData setObject:[dictCurrent objectForKey:@"TblMaincontent"] forKey:@"item_name"];
        [dictData setObject:[dictCurrent objectForKey:@"value"] forKey:@"qty"];
        [dictData setObject:[dictCurrent objectForKey:@"dollOne"] forKey:@"unit_price"];
        
        if ([dictCurrent objectForKey:@"itemDescription"]) {
            [dictData setObject:[dictCurrent objectForKey:@"itemDescription"] forKey:@"itemDescription"];
        }
        
        if ([dictCurrent objectForKey:@"image"]) {
            [dictData setObject:[dictCurrent objectForKey:@"image"] forKey:@"image"];
        }
        
        if ([dictCurrent objectForKey:@"SplRqst"]) {
            [dictData setObject:[dictCurrent objectForKey:@"SplRqst"] forKey:@"special_request"];
        } else {
            [dictData setObject:@"" forKey:@"special_request"];
        }

        [ArrToServer addObject:dictData];
    }
    NSDate *Now=[NSDate date];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSString *StrDate=[dateFormatter stringFromDate:Now];
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    NSString *StrDonoSelection=[prefs objectForKey:@"DonorSelection"];


    if ([StrDonoSelection isEqualToString:@"DonorSelected"]) {
        if (_donorDetails) {
            if([ArrToServer count]) {
                NSString *totalAmntOftransaction=[SRUtility getfromplist:@"TrabsactionTotalAmnt" plist:@"Data"];
                NSUserDefaults *Prefs=[NSUserDefaults standardUserDefaults];
                NSString *Userpin=[Prefs objectForKey:@"UserPinForLogin"];
                NSMutableArray *ArrDonations=[[NSMutableArray alloc]init];
                NSMutableDictionary *dicDonors=[[NSMutableDictionary alloc]init];
                [dicDonors setObject:ArrToServer forKey:@"donations"];
                [dicDonors setObject:StrDate forKey:@"date_time"];
                [dicDonors setObject:totalAmntOftransaction forKey:@"grand_total"];
                [dicDonors setObject:Userpin forKey:@"user_pin"];
                [dicDonors setObject:[_donorDetails objectForKey:@"email"] forKey:@"donor_email"];
                [dicDonors setObject:[_donorDetails objectForKey:@"donor_name"] forKey:@"donor_name"];
                [dicDonors setObject:@"" forKey:@"transactionID"];
                [ArrDonations addObject:dicDonors];
                
                NSDictionary *dictToPass=[[NSDictionary alloc]initWithObjectsAndKeys:ArrDonations,@"donors",[_donorDetails objectForKey:@"pin_nbr"],@"pin_nbr",@"0",@"server_recieved", nil];
                [ArrForSavingData addObject:dictToPass];
                NSData *data=[NSKeyedArchiver archivedDataWithRootObject:ArrForSavingData];
                [SRUtility adddatatoplist:data key:@"OfflineTransaction" plist:@"Data"];
                NSString *StrUsrPin=[NSString stringWithFormat:@"%@",[dictToPass objectForKey:@"pin_nbr"]];
                NSString *StrTempId=[NSString stringWithFormat:@"%@",[[[dictToPass objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"grand_total"]];


                for (int i=0; i<[ArrToSave count]; i++) {
                    if ([[[[[[ArrToSave objectAtIndex:i] objectAtIndex:0] objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"grand_total"] isEqualToString:StrTempId]&&[[[ArrToSave objectAtIndex:i] objectAtIndex:0] objectForKey:StrUsrPin])
                    {
                        NSMutableArray *ArrData=[[NSMutableArray alloc] init];
                        [ArrData addObject:dictToPass];
                        [ArrToSave removeObjectAtIndex:i];
                        [ArrToSave insertObject:ArrData atIndex:i];
                    }
                }
                NSData *dataOfflineAfterSave=[NSKeyedArchiver archivedDataWithRootObject:ArrToSave];
                [SRUtility adddatatoplist:dataOfflineAfterSave key:@"Donations" plist:@"Data"];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AREarlyLearning" message:@"Transaction saved." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
                NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
                [prefs setObject:@"DonorNotSelected" forKey:@"DonorSelection"];
                [prefs synchronize];
                [_labltrasactionId setHidden:YES];
                [_LblGrandTotal setText:@"0.00"];
                [_LblDonorName setHidden:YES];
                ArrTableData=[[NSMutableArray alloc]init];
                [TblViewDonation reloadData];
            } else {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Please select any items." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
        } else {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Please select a donor." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    } else {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"AR Early Learning" message:@"Please select a donor." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    [self performSelector:@selector(BtnCategoryClicked:) withObject:nil afterDelay:0.0];
}

-(void)reloadTableFromDailyReports:(NSDictionary*)ArrFromDailyReports arrData:(NSArray*)ArrdailyReport
{
    NSLog(@"reloadTableFromDailyReports");
    savedTransaction=YES;
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    [prefs setObject:@"DonorSelected" forKey:@"DonorSelection"];
    [prefs synchronize];
    NSData *DataOffline=[SRUtility getdatafromplist:@"OfflineTransaction" plist:@"Data"];
    ArrForSavingData=[NSKeyedUnarchiver unarchiveObjectWithData:DataOffline];
    //[_btnSave setHidden:YES];
    
    if (![DataOffline length]||[ArrForSavingData count]==0) {
        ArrForSavingData=[[NSMutableArray alloc]init];
        
    }
    
    ArrTableData=[[NSMutableArray alloc]init];
    for (NSDictionary *dictCurrent in [[[ArrFromDailyReports objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"donations"]) {
        NSMutableDictionary *dictData=[[NSMutableDictionary alloc]init];
        [dictData setObject:[dictCurrent objectForKey:@"item_id"] forKey:@"item_id"];
        [dictData setObject:[dictCurrent objectForKey:@"item_name"] forKey:@"TblMaincontent"];
        [dictData setObject:[dictCurrent objectForKey:@"unit_price"] forKey:@"dollOne"];
        [dictData setObject:[dictCurrent objectForKey:@"qty"] forKey:@"value"];
        
        if ([dictCurrent objectForKey:@"image"]) {
            [dictData setObject:[dictCurrent objectForKey:@"image"] forKey:@"image"];
            
        }
        
        if ([dictCurrent objectForKey:@"itemDescription"]) {
            [dictData setObject:[dictCurrent objectForKey:@"itemDescription"] forKey:@"itemDescription"];
        }
        
        if ([dictCurrent objectForKey:@"special_request"]) {
            [dictData setObject:[dictCurrent objectForKey:@"special_request"] forKey:@"SplRqst"];
        }
        
        
        [ArrTableData addObject:dictData];
    }
    //[ObjDonDetailedView.specialRequestTxt setText:[]];
    NSData *dataDonors=[SRUtility getdatafromplist:@"Donors" plist:@"Data"];
    NSDictionary *DicDonors=[NSKeyedUnarchiver unarchiveObjectWithData:dataDonors];
    NSMutableArray *ArrDonors=[DicDonors objectForKey:@"data"];
    if ([ArrFromDailyReports objectForKey:@"donors"]) {
        NSPredicate *thepredicate=[NSPredicate predicateWithFormat:@"SELF.pin_nbr=%@",[ArrFromDailyReports objectForKey:@"pin_nbr"]];
        NSArray *ARrDonorFltrd=[ArrDonors filteredArrayUsingPredicate:thepredicate];
        _donorDetails=[ARrDonorFltrd objectAtIndex:0];
        [_LblDonorName setText:[[ARrDonorFltrd objectAtIndex:0] objectForKey:@"donor_name"]];
        NSUserDefaults *Objprefs=[NSUserDefaults standardUserDefaults];
        [Objprefs setObject:@"DonorSelected" forKey:@"DonorSelection"];
        [Objprefs synchronize];
        [_LblDonorName setHidden:NO];
        [self TotalSum];
        [TblViewDonation reloadData];
        NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
        [prefs setObject:@"reloaded" forKey:@"FromTransaction"];
        [prefs synchronize];
        [self performSelector:@selector(BtnCategoryClicked:) withObject:nil afterDelay:0.3];
        NSString *StrDatenTimeTopass=[[[ArrFromDailyReports objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"date_time"];
        
        for (NSDictionary *dictCurrent in ArrdailyReport) {
            if ([[[[dictCurrent objectForKey:@"donors"] objectAtIndex:0] objectForKey:@"date_time"] isEqualToString:StrDatenTimeTopass]) {
                [ArrForSavingData removeObject:dictCurrent];
            }
        }
       
        NSData *data=[NSKeyedArchiver archivedDataWithRootObject:ArrForSavingData];
        [SRUtility adddatatoplist:data key:@"OfflineTransaction" plist:@"Data"];
        //[ArrForSavingData removeLastObject];
        //[SRUtility add];
        
    }  else {
      [_LblDonorName setHidden:YES];
      [self GrandTotal];
      [TblViewDonation reloadData];
      [self dismissViewControllerAnimated:YES completion:Nil];
    }
}

-(void)TotalSum
{
    NSLog(@"TotalSum");
    float GrandTotal=0;
    for (NSDictionary *dictCurrent in ArrTableData)
    {
        NSString *strDollOne=[NSString stringWithFormat:@"%@",[dictCurrent objectForKey:@"dollOne"]];
        NSString *strValue=[NSString stringWithFormat:@"%@",[dictCurrent objectForKey:@"value"]];
        float dollOne=[strDollOne floatValue];
        float value=[strValue floatValue];
        GrandTotal=dollOne*value;
        NSString *StrDollTwo=[NSString stringWithFormat:@"%.2f",GrandTotal];
        [dictCurrent setValue:StrDollTwo forKey:@"dollTwo"];
    }
}

-(void)CallTransaction
{
    NSLog(@"CallTransaction");
     NSDictionary *transaction=[SRUtility makeWebServicecallForGetmethod:[NSURL URLWithString:[NSString stringWithFormat:@"http://donations.arearlylearning.org/donation-api-test/index.php?request=transaction"]]];

    NSDecimalNumber *transactionID=transaction[@"data"];
    stringOFTransaction=[NSString stringWithFormat:@"%@",transactionID];
    if([stringOFTransaction isEqualToString:@"(null)"])
    {
        _labltrasactionId.text=@"";
    }
    else
    {
        [_labltrasactionId setText:stringOFTransaction];
    }
}

@end
